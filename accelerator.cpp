#include "accelerator.h"
#include <scene/transform.h>
#include <scene/light.h>
#include <scene/shape.h>
#include <scene/sceneio.h>

// common routines --------
namespace AcceleratorCommon {
std::vector<AcceleratorSurface*> buildSurfaces(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights,
                                          bool refine) {
    std::vector<AcceleratorSurface*> asurfaces;
    for(int i = 0; i < surfaces.size(); i ++) {
        if (refine) load(surfaces[i]->shape);
        asurfaces.push_back(new AcceleratorSurface(surfaces[i]->shape, 
                                                   surfaces[i]->transform, 
                                                   surfaces[i]->material,
                                                   surfaces[i]));
    }
    // check that these are all triangles and rigid maps
    for(int i = 0; i < lights.size(); i ++) {
        switch (lights[i]->type) {
            case AreaLight::TYPEID:
                if (refine) load(((AreaLight*)lights[i])->shape);
                asurfaces.push_back(new AcceleratorSurface(((AreaLight*)lights[i])->shape,
                                                           lights[i]->transform, 
                                                           ((AreaLight*)lights[i])->material,
                                                           lights[i])); 
                break;
            default: break;
        }
    }
    // this is to handle the option using or not vectors
    return asurfaces;
}

void intersectResolve(const sintersection3f& ti, const AcceleratorSurface* surface, const ray3f& ray, intersection3f& intersection) {
    switch(surface->shape->type) {
        case TriangleMeshShape::TYPEID: {
            TriangleMeshShape* mesh = (TriangleMeshShape*)(surface->shape);
            const vec3i& f = mesh->face[ti.idx];
            const vec3f& v0 = mesh->pos[f.x];
            const vec3f& v1 = mesh->pos[f.y];
            const vec3f& v2 = mesh->pos[f.z];
            intersection.t = ti.t;
            intersection.f.o = ti.p;
            if(mesh->norm.empty()) intersection.f.z = triangleNormal(v0,v1,v2);
            else intersection.f.z = normalize(lerp(mesh->norm[f.x],mesh->norm[f.y],mesh->norm[f.z],ti.uv));
            xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
            if(mesh->uv.empty()) intersection.st = ti.uv;
            else intersection.st =lerp(mesh->uv[f.x],mesh->uv[f.y],mesh->uv[f.z],ti.uv);
        } break;
        case QuadShape::TYPEID: {
            // QuadShape* quad = (QuadShape*)(surface->shape);
            intersection.t = ti.t;
            intersection.f.o = ti.p;
            intersection.f.z = z3f;
            intersection.f.x = x3f;
            intersection.f.y = y3f;
            intersection.st = ti.uv;
        } break;
        case SphereShape::TYPEID: {
            SphereShape* sphere = (SphereShape*)(surface->shape);
            intersection.t = ti.t;
            intersection.f.o = ti.p;
            intersection.f.z = intersection.f.o / sphere->radius;
            if (dot(intersection.f.z, ray.d) > 0) intersection.f.z = -intersection.f.z;
            xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
            intersection.st = ti.uv;
        } break;
        case HairMeshShape::TYPEID: {
            HairMeshShape* mesh = (HairMeshShape*)(surface->shape);
            const vec3i& f = mesh->face[ti.idx];
            intersection.t = ti.t;
            intersection.f.o = ti.p;
            intersection.f.z = normalize(lerp(mesh->tangent[f.x],mesh->tangent[f.y],mesh->tangent[f.z],ti.uv));
            xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
            if(mesh->uv.empty()) intersection.st = ti.uv;
            else intersection.st =lerp(mesh->uv[f.x],mesh->uv[f.y],mesh->uv[f.z],ti.uv);
        } break;
        default: error("unknown shape type");
    }
    intersection.f = transformFrame(surface->transform, intersection.f);
    if (dot(intersection.f.z, ray.d) > 0 && 
        surface->shape->type == TriangleMeshShape::TYPEID) {
        intersection.f = flip(intersection.f);
    }
    intersection.m = surface->material;
}

static vec2f resolveTexcoord(const Shape *shape, int idx, const vec2f &uv) {
    switch(shape->type) {
        case TriangleMeshShape::TYPEID:
        case HairMeshShape::TYPEID: {
            TriangleMeshShape* mesh = (TriangleMeshShape*)(shape);
            if(mesh->uv.empty()) return uv;
            const vec3i& f = mesh->face[idx];
            return lerp(mesh->uv[f.x],mesh->uv[f.y],mesh->uv[f.z],uv);
        } break;
        case QuadShape::TYPEID:
        case SphereShape::TYPEID:
        default: return uv;
	}
}

bool opacityResolve(const Texture* opacity, const Shape* shape, int idx, const vec2f &uv) {
    if (!opacity) return true;
    if (opacity->image.size() == 0) return true;

    vec2f st = resolveTexcoord(shape, idx, uv);

    int i = static_cast<int>(st.x * opacity->image.width());
    int j = static_cast<int>(st.y * opacity->image.height());
    int w = opacity->image.width();
    int h = opacity->image.height();

    if (i < 0 || i >= w || j < 0 || j >= h) return false;
    return !opacity->image.at(i,h-j-1).iszero();
}

bool intersectFirst(const Shape* shape, const Texture* opacity, const ray3f& ray, sintersection3f& intersection) {
    switch(shape->type) {
        case TriangleMeshShape::TYPEID: 
        case HairMeshShape::TYPEID: {
            const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
            ray3f tray = ray; bool hit = false;
            for(int i = 0; i < mesh->face.size(); i ++) {
                const vec3i& f = mesh->face[i];
                const vec3f& v0 = mesh->pos[f.x];
                const vec3f& v1 = mesh->pos[f.y];
                const vec3f& v2 = mesh->pos[f.z];
                float t; vec2f uv;
                bool th = intersectTriangle(v0,v1,v2,tray,t,uv);
                if(th && (!hit || intersection.t > t)) {
                    if(opacityResolve(opacity, shape, i, uv)) {
                        hit = true;
                        intersection.t = t;
                        intersection.p = ray.eval(t);
                        intersection.uv = uv;
                        intersection.idx = i;
                        tray.maxt = intersection.t;
                    }
                }
            }
            return hit;
        } break;
        case QuadShape::TYPEID: {
            const QuadShape* quad = (const QuadShape*)shape;
            float t; vec2f uv;
            bool th = intersectQuad(quad->width,quad->height,ray,t,uv);
            if(th && opacityResolve(opacity, shape, 0, uv)) {
                intersection.t = t;
                intersection.p = ray.eval(t);
                intersection.uv = uv;
                intersection.idx = 0;
            }
            return th;
        } break;
        case SphereShape::TYPEID: {
            const SphereShape* sphere = (const SphereShape*)shape;
            float t; vec2f uv;
            bool th = intersectSphere(zero3f,sphere->radius,ray,t,uv);
            if(th && opacityResolve(opacity, shape, 0, uv)) {
                intersection.t = t;
                intersection.p = ray.eval(t);
                intersection.uv = uv;
                intersection.idx = 0;
            }
            return th;
        } break;
        default: error("unknown shape type"); return false;
    }
}

bool intersectFirst(const AcceleratorSurface* surface, const ray3f& ray,
                    intersection3f& intersection) {
    ray3f tray = transformRayInverse(surface->transform, ray);
    sintersection3f si;
    bool hit = intersectFirst(surface->shape, surface->material->opacity, tray, si);
    if(hit) intersectResolve(si, surface, ray, intersection);
    return hit;
}


bool intersectAny(const Shape* shape, const Texture* opacity, const ray3f& ray) {
    switch(shape->type) {
        case TriangleMeshShape::TYPEID: 
        case HairMeshShape::TYPEID: {
            const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
            for(int i = 0; i < mesh->face.size(); i ++) {
                const vec3i& f = mesh->face[i];
                const vec3f& v0 = mesh->pos[f.x];
                const vec3f& v1 = mesh->pos[f.y];
                const vec3f& v2 = mesh->pos[f.z];
                float t; vec2f uv;
                bool th = intersectTriangle(v0,v1,v2,ray,t,uv);
                if(th && opacityResolve(opacity, shape, i, uv)) return true;
            }
            return false;
        } break;
        case QuadShape::TYPEID: {
            const QuadShape* quad = (const QuadShape*)shape;
            float t; vec2f uv;
            bool th = intersectQuad(quad->width,quad->height,ray,t,uv);
            return th && opacityResolve(opacity, shape, 0, uv);
        } break;
        case SphereShape::TYPEID: {
            const SphereShape* sphere = (const SphereShape*)shape;
            float t; vec2f uv;
            bool th = intersectSphere(zero3f,sphere->radius,ray,t,uv);
            return th && opacityResolve(opacity, shape, 0, uv);
        } break;
        default: error("unknown shape type"); return false;
    }
}


range3f computeWorldBound(const Shape* shape, const Transform *transform) {
    range3f bbox = invalidrange3f;
    switch(shape->type) {
        case TriangleMeshShape::TYPEID:
        case HairMeshShape::TYPEID: {
            const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
            for(int i = 0; i < mesh->face.size(); i ++) {
                const vec3i& f = mesh->face[i];
                const vec3f v0 = transformPoint(transform, mesh->pos[f.x]);
                const vec3f v1 = transformPoint(transform, mesh->pos[f.y]);
                const vec3f v2 = transformPoint(transform, mesh->pos[f.z]);
                bbox.grow(triangleBoundingBox(v0, v1, v2));
            }
        } break;
        case QuadShape::TYPEID: {
            const QuadShape* quad = (const QuadShape*)(shape);
            float w = quad->width / 2.0f;
            float h = quad->height / 2.0f;
            bbox.grow(transformPoint(transform, vec3f(-w, -h, 0)));
            bbox.grow(transformPoint(transform, vec3f(-w,  h, 0)));
            bbox.grow(transformPoint(transform, vec3f( w,  h, 0)));
            bbox.grow(transformPoint(transform, vec3f( w, -h, 0)));
        } break;
        case SphereShape::TYPEID: {
            const SphereShape* sphere = (const SphereShape*)(shape);
            float r = sphere->radius;
            vec3f center = transformPoint(transform, vec3f(0, 0, 0));
            bbox = range3f(center + vec3f(-r, -r, -r), center + vec3f(r, r, r));
        } break;
        default: error("unknown shape type"); break;
    }
    return bbox;
}

range3f computeWorldBound(const AcceleratorSurface* surface) { return computeWorldBound(surface->shape, surface->transform); }
range3f computeWorldBound(const Surface* surface) { return computeWorldBound(surface->shape, surface->transform); }
template<typename S>
range3f computeWorldBound(const std::vector<S*> &surfaces) {
    range3f bbox = invalidrange3f;
    for(int i = 0; i < surfaces.size(); i ++) {
        bbox.grow(computeWorldBound(surfaces[i]));
    }
    return bbox;
}
range3f computeWorldBound(const std::vector<AcceleratorSurface*> &surfaces) { return computeWorldBound<AcceleratorSurface>(surfaces); }
range3f computeWorldBound(const std::vector<Surface*> &surfaces) { return computeWorldBound<Surface>(surfaces); }

}
