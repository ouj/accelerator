#include "accelerator_lux.h"
#include <scene/transform.h>
#include <scene/shape.h>
#include <accelerator/luxrays/accelerators/bvhaccel.h>
#include <accelerator/luxrays/accelerators/qbvhaccel.h>

using std::string;
using namespace luxrays;

LuxAcceleratorOpts::LuxAcceleratorOpts() : AcceleratorOpts(LuxAccelerator::TYPEID) {}

jsonObject LuxAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "luxacceleratoropts";
    if(luxtype == BVH) json["luxtype"] = "bvh";
    else if(luxtype == QBVH) json["luxtype"] = "qbvh";
    else error("unknown lux accelerator type");
    return json;
}

void LuxAcceleratorOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("luxacceleratoropts");
    string ltype = jsonGet(json, "luxtype");
    if(ltype == "bvh") luxtype = BVH;
    else if(ltype == "qbvh") luxtype = QBVH;
    else error("unknown lux accelerator type");
}

TriangleMeshShape* merge(int totalVertexCount, int totalTriangleCount,
                         const std::vector<AcceleratorSurface*>& surfaces,
                         int **preprocessedMeshIDs,
                         int **preprocessedMeshTriangleIDs) {
    TriangleMeshShape* ret = new TriangleMeshShape();
    ret->pos.resize(totalVertexCount);
    ret->face.resize(totalTriangleCount);

    if (preprocessedMeshIDs)
        *preprocessedMeshIDs = new int[totalTriangleCount];
    if (preprocessedMeshTriangleIDs)
        *preprocessedMeshTriangleIDs = new int[totalTriangleCount];

    int pIndex = 0;
    int fIndex = 0;
    for (int i = 0; i < surfaces.size(); i ++) {
        error_if_not(surfaces[i]->shape->type == TriangleMeshShape::TYPEID,
                     "supports only triangle meshes");
        const TriangleMeshShape* mesh =  (TriangleMeshShape*)surfaces[i]->shape;

        for(int j = 0; j < mesh->pos.size(); j ++) {
            ret->pos[j+pIndex] = transformPoint(surfaces[i]->transform, mesh->pos[j]);
        }

        // Translate mesh indices
        for (int j = 0; j < mesh->face.size(); j++) {
            ret->face[j+fIndex] = mesh->face[j] + vec3<int>(pIndex,pIndex,pIndex);
            if (preprocessedMeshIDs)
                (*preprocessedMeshIDs)[j+fIndex] = i;
            if (preprocessedMeshTriangleIDs)
                (*preprocessedMeshTriangleIDs)[j+fIndex] = j;
        }

        pIndex += mesh->pos.size();
        fIndex += mesh->face.size();
    }
    return ret;
}

TriangleMeshShape *merge(const std::vector<AcceleratorSurface*> &surfaces,
                         int **preprocessedMeshIDs,
                         int **preprocessedMeshTriangleIDs) {
    int totalVertexCount = 0;
    int totalTriangleCount = 0;

    for (int i = 0; i < surfaces.size(); i ++) {
        error_if_not(surfaces[i]->shape->type == TriangleMeshShape::TYPEID,
                     "only triangle meshes supported");
        TriangleMeshShape* mesh = (TriangleMeshShape*)surfaces[i]->shape;
        totalVertexCount += mesh->pos.size();
        totalTriangleCount += mesh->face.size();
    }
    return merge(totalVertexCount, totalTriangleCount, surfaces,
                 preprocessedMeshIDs, preprocessedMeshTriangleIDs);
}

TriangleMesh* merge_tolux(const std::vector<AcceleratorSurface*>& surfaces,
                          int **preprocessedMeshIDs,
                          int **preprocessedMeshTriangleIDs) {
    TriangleMeshShape* mesh = merge(surfaces,
                                    preprocessedMeshIDs,
                                    preprocessedMeshTriangleIDs);
    Point* pos = new Point[mesh->pos.size()];
    for(int i = 0; i < mesh->pos.size(); i ++) {
        pos[i] = Point(mesh->pos[i].x, mesh->pos[i].y, mesh->pos[i].z);
    }
    Triangle* tris = new Triangle[mesh->face.size()];
    for(int i = 0; i < mesh->face.size(); i ++) {
        tris[i] = Triangle(mesh->face[i].x, mesh->face[i].y, mesh->face[i].z);
    }
    TriangleMesh* luxmesh = new TriangleMesh(mesh->pos.size(),mesh->face.size(),pos,tris);
    delete mesh;
    return luxmesh;
}

LuxAccelerator::LuxAccelerator() : Accelerator(TYPEID) {}

LuxAccelerator::LuxAccelerator(const LuxAcceleratorOpts& opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

bool LuxAccelerator::buildAccelerator(const std::vector<Surface*>& surfaces,
                                      const std::vector<Light*>& lights) {
    std::vector<Surface*> meshSurfaces;
    std::vector<Surface*> otherSurfaces;

    for(int i = 0; i < surfaces.size(); i ++) {
        if(surfaces[i]->shape->type == TriangleMeshShape::TYPEID) {
            meshSurfaces.push_back(surfaces[i]);
        } else {
            otherSurfaces.push_back(surfaces[i]);
        }
    }
    otherAccelerator = new ListAccelerator();
    otherAccelerator->buildAccelerator(otherSurfaces, lights);

    this->meshSurfaces = AcceleratorCommon::buildSurfaces(surfaces, std::vector<Light*>(), true);
    meshBbox = AcceleratorCommon::computeWorldBound(this->meshSurfaces);

    TriangleMesh* mesh = merge_tolux(this->meshSurfaces, &meshIds, &triangleIds);
    // build mesh bvh
    if(_opts.luxtype == LuxAcceleratorOpts::BVH) {
        meshAccelerator = new BVHAccel(0, 4, 16, 80, 10, 0.5f);
        std::deque<Mesh*> q; q.push_back(mesh);
        meshAccelerator->Init(q, mesh->GetTotalVertexCount(), mesh->GetTotalTriangleCount());
    } else if(_opts.luxtype == LuxAcceleratorOpts::QBVH) {
        meshAccelerator = new QBVHAccel(0, 4, 16, 1);
        std::deque<Mesh*> q; q.push_back(mesh);
        meshAccelerator->Init(q, mesh->GetTotalVertexCount(), mesh->GetTotalTriangleCount());
    } else error("unknown lux accelerator type");
    delete mesh;
    return true;
}

void LuxAccelerator::clearAccelerator() {
    if (otherAccelerator) delete otherAccelerator;
    if (meshAccelerator) delete meshAccelerator;
    if (meshIds) delete [] meshIds;
    if (triangleIds) delete [] triangleIds;
}

bool LuxAccelerator::intersectFirst(const ray3f& ray, sintersection3f& intersection, AcceleratorSurface*& surface) const {
    bool hit  = false;

    sintersection3f otherIntersection; AcceleratorSurface* otherSurface;
    bool hitOther = otherAccelerator->intersectFirst(ray, otherIntersection, otherSurface);
    if(hitOther) {
        hit = true;
        intersection = otherIntersection;
        surface = otherSurface;
    }

    RayHit rayhit; rayhit.SetMiss();
    Ray lray(Point(ray.o.x,ray.o.y,ray.o.z), Vector(ray.d.x,ray.d.y,ray.d.z), ray.mint, ray.maxt);
    bool hitTriangle = meshAccelerator->Intersect(&lray, &rayhit, false);
    if(hitTriangle && (!hit || rayhit.t < intersection.t)) {
        surface = meshSurfaces[meshIds[rayhit.index]];
        hit = true;
        intersection.t = rayhit.t;
        intersection.p = transformPointInverse(surface->transform,ray.eval(intersection.t));
        intersection.uv = vec2f(rayhit.b1,rayhit.b2);
        intersection.idx = triangleIds[rayhit.index];
    }

    return hit;
}

bool LuxAccelerator::intersectFirst(const ray3f& ray, intersection3f& intersection) const {
    sintersection3f si; AcceleratorSurface* surface;
    bool hit = intersectFirst(ray, si, surface);
    if(hit) AcceleratorCommon::intersectResolve(si, surface, ray, intersection);
    return hit;
}


bool LuxAccelerator::intersectAny(const ray3f& ray) const {
    if(otherAccelerator->intersectAny(ray)) return true;
    RayHit rayhit;  rayhit.SetMiss();
    Ray lray(Point(ray.o.x,ray.o.y,ray.o.z),
             Vector(ray.d.x,ray.d.y,ray.d.z),ray.mint,ray.maxt);
    return meshAccelerator->Intersect(&lray, &rayhit, true);
}

range3f LuxAccelerator::computeWorldBound() const {
    return runion(meshBbox, otherAccelerator->computeWorldBound());
}

jsonObject LuxAccelerator::statistics() const {
    return jsonObject();
}
