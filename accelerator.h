#ifndef _ACCELERATOR_H
#define _ACCELERATOR_H

// a simple list intersection group supporting triangle surfaces
#include <scene/scene.h>
#include <common/json.h>

// intersection ------------------------------------

template<typename R>
struct intersection3 {
    intersection3() : m(nullptr) {}
    R                   t;
    frame3<R>           f;
    vec2<R>             st;
    const Material*     m;
};

typedef intersection3<float> intersection3f;
typedef intersection3<double> intersection3d;

template<typename R>
struct sintersection3 {
    R           t;
    vec3<R>     p;
    vec2<R>     uv;
    int         idx;
};

typedef sintersection3<float> sintersection3f;
typedef sintersection3<double> sintersection3d;

// accelerator surface ------------------------------

struct AcceleratorAux {};
struct AcceleratorSurface {
    Transform*          transform;
    Material*           material;
    Shape*              shape;
    AcceleratorAux*     aux;
    AcceleratorSurface(Shape* s, Transform* t, Material* m, const void* so) :
    transform(t), material(m), shape(s), aux(nullptr) { }
};

struct AcceleratorOpts {
    AcceleratorOpts(int t) : type(t) {}
    virtual ~AcceleratorOpts() {}
    virtual jsonObject  print() const = 0;
    virtual void        parse(const jsonObject &opts) = 0;

    int                 type;
};

// accelerator --------------------------------------
struct Accelerator {
    int type;
    Accelerator(int t) { type = t; }
    virtual ~Accelerator() {};

    // dispatch ------------------------------
    virtual bool        intersectFirst(const ray3f& ray, intersection3f& intersection) const = 0;
    virtual bool        intersectAny(const ray3f& ray) const = 0;
    virtual range3f     computeWorldBound() const = 0;

    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights) = 0;
    virtual void        clearAccelerator() = 0;

    virtual jsonObject  statistics() const = 0;
};


namespace AcceleratorCommon {
    // static intersection functions -------------------------
    bool intersectFirst(const Shape* shape, const Texture* opacity, const ray3f& ray, sintersection3f& intersection);
    bool intersectAny(const Shape* shape, const Texture* opacity, const ray3f& ray);
    bool intersectFirst(const AcceleratorSurface* surface, const ray3f& ray, intersection3f& intersection);
    void intersectResolve(const sintersection3f& ti, const AcceleratorSurface* surface, const ray3f& ray, intersection3f& intersection);
    bool opacityResolve(const Texture* opacity, const Shape* shape, int idx, const vec2f &uv);
    // common -------------------------------
    std::vector<AcceleratorSurface*> buildSurfaces(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights, bool refine);
    range3f computeWorldBound(const AcceleratorSurface *surface);
    range3f computeWorldBound(const Surface *surface);
    range3f computeWorldBound(const std::vector<AcceleratorSurface*> &surfaces);
    range3f computeWorldBound(const std::vector<Surface*> &surfaces);
    range3f computeWorldBound(const Shape* shape, const Transform *transform);
}

#endif
