#include "accelerator_bvh.h"
#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>

static sbimap<BvhSplitMode> splitmode_b = {
    { "even", SPLIT_EVEN }, { "middle", SPLIT_MIDDLE }, { "sah", SPLIT_SAH }
};

BvhAcceleratorOpts::BvhAcceleratorOpts() : AcceleratorOpts(BvhAccelerator::TYPEID) {}

jsonObject BvhAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "bvhacceleratoropts";
    json["maxleafitems"] = maxLeafItems;
    json["surfacesplitmode"] = jsonFromEnum(surfaceSplitMode, splitmode_b);
    json["shapesplitmode"] = jsonFromEnum(shapeSplitMode, splitmode_b);
    json["parallel"] = parallel;
    return json;
}

void BvhAcceleratorOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("bvhacceleratoropts");

    maxLeafItems = jsonGet(json, "maxleafitems");
    surfaceSplitMode = jsonToEnum(jsonGet(json, "surfacesplitmode"), splitmode_b);
    shapeSplitMode = jsonToEnum(jsonGet(json, "shapesplitmode"), splitmode_b);
    parallel = jsonGet(json, "parallel");
}

const int TRAVERSAL_STACK_SIZE = 64;
using namespace BVH;

BvhAccelerator::BvhAccelerator(): Accelerator(TYPEID) {}

BvhAccelerator::BvhAccelerator(const BvhAcceleratorOpts& opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

BvhAccelerator::~BvhAccelerator() { clearAccelerator(); }

bool BvhAccelerator::buildAccelerator(const std::vector<Surface*>& surfs,
                                      const std::vector<Light*>& lights) {
    this->surfaces = AcceleratorCommon::buildSurfaces(surfs, lights, true);
    if (this->surfaces.size() == 0) {
        _worldBound = range3f(zero3f, zero3f);
        return true;
    } else {
        buildBvhAccelerator();
        return true;
    }
}

template<typename SHAPE>
inline bool intersectSubShape(const SHAPE* shape, int index,
                              const ray3f& ray, float& t, vec2f& uv) {
    const vec3i &face = shape->face[index];
    const vec3f &v0 = shape->pos[face.x];
    const vec3f &v1 = shape->pos[face.y];
    const vec3f &v2 = shape->pos[face.z];
    return intersectTriangle(v0,v1,v2,ray,t,uv);
}

template<typename SHAPE>
bool intersectFirst(const Shape* sh, const AcceleratorAux* aux,
                    Texture* opacity, const ray3f &rr, sintersection3f& intersection) {
    if(aux == 0) return false;
    const SHAPE* shape = static_cast<const SHAPE*>(sh);
    const SurfaceBvh *bvh = static_cast<const SurfaceBvh*>(aux);
    int nodeNum = 0;

    ray3f ray = rr;
    std::array<int, TRAVERSAL_STACK_SIZE> stack;
    bool hit = false;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    while (true) {
        const LinearBvhNode *node = &(bvh->nodes[nodeNum]);
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) {
                for (int s = 0; s < node->nitems ; s++) {
                    int index = bvh->indices[node->offset + s];
                    float t; vec2f uv;
                    bool th = intersectSubShape(shape, index, ray, t, uv);
                    if(th && (!hit || intersection.t > t)) {
                        if (AcceleratorCommon::opacityResolve(opacity, shape, index, uv)) {
                            hit = true;
                            intersection.t = t;
                            intersection.p = ray.eval(t);
                            intersection.uv = uv;
                            intersection.idx = index;
                            ray.maxt = intersection.t;
                        }
                    }
                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->secondChild;
                } else {
                    stack[sptr++] = node->secondChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return hit;
}

bool BvhAccelerator::intersectFirst(const ray3f &rr,
                                    sintersection3f& intersection,
                                    AcceleratorSurface*& surface) const {
    if(nodes.empty()) return false;
    int nodeNum = 0;

    ray3f ray = rr;
    std::array<int, TRAVERSAL_STACK_SIZE> stack;

    bool hit = false;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    while (true) {
        const LinearBvhNode *node = &nodes[nodeNum];
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) {
                for (int s = 0; s < node->nitems ; s++) {
                    AcceleratorSurface *asurface = surfaces[node->offset + s];
                    ray3f tray = transformRayInverse(asurface->transform, ray);
                    bool sh = false; sintersection3f si;
                    switch (asurface->shape->type) {
                        case TriangleMeshShape::TYPEID:
                            sh = ::intersectFirst<TriangleMeshShape>(asurface->shape, asurface->aux,
                                                                     asurface->material->opacity, tray, si);
                            break;
                        case HairMeshShape::TYPEID:
                            sh = ::intersectFirst<HairMeshShape>(asurface->shape, asurface->aux,
                                                                 asurface->material->opacity, tray, si);
                            break;
                        default:
                            sh = AcceleratorCommon::intersectFirst(asurface->shape, asurface->material->opacity, tray, si);
                            break;
                    }
                    if(sh && (si.t < intersection.t || !hit)) {
                        hit = true;
                        intersection = si;
                        surface = asurface;
                        ray.maxt = intersection.t;
                    }
                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->secondChild;
                } else {
                    stack[sptr++] = node->secondChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return hit;
}

bool BvhAccelerator::intersectFirst(const ray3f& ray, intersection3f& intersection) const {
    sintersection3f si; AcceleratorSurface* surface;
    bool hit = intersectFirst(ray, si, surface);
    if(hit) AcceleratorCommon::intersectResolve(si, surface, ray, intersection);
    return hit;
}

template<typename SHAPE>
bool intersectAny(const Shape* sh, const AcceleratorAux * aux,
                  const Texture *opacity, const ray3f& ray) {
    if(aux == 0) return false;
    const SHAPE* shape = static_cast<const SHAPE*>(sh);
    const SurfaceBvh *bvh = static_cast<const SurfaceBvh*>(aux);

    int nodeNum = 0;
    std::array<int, TRAVERSAL_STACK_SIZE> stack;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    while (true) {
        const LinearBvhNode *node = &(bvh->nodes[nodeNum]);
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) {
                for (int s = 0; s < node->nitems ; s++) {
                    int index = bvh->indices[node->offset + s];
                    float t; vec2f uv;
                    bool th = intersectSubShape(shape, index, ray, t, uv);
                    if(th && AcceleratorCommon::opacityResolve(opacity, shape, index, uv)) return true;
                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->secondChild;
                } else {
                    stack[sptr++] = node->secondChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return false;
}

bool BvhAccelerator::intersectAny(const ray3f& ray) const {
    if(nodes.empty()) return false;

    std::array<int, TRAVERSAL_STACK_SIZE> stack;
    int nodeNum = 0;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    while (true) {
        const LinearBvhNode *node = &nodes[nodeNum];
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) {
                for (int s = 0; s < node->nitems ; s++) {
                    const AcceleratorSurface* surface = surfaces[node->offset + s];
                    ray3f tray = transformRayInverse(surface->transform, ray);
                    switch (surface->shape->type) {
                        case TriangleMeshShape::TYPEID:
                            if (::intersectAny<TriangleMeshShape>(surface->shape, surface->aux,
                                                                  surface->material->opacity, tray)) return true;
                            break;
                        case HairMeshShape::TYPEID:
                            if (::intersectAny<HairMeshShape>(surface->shape, surface->aux,
                                                              surface->material->opacity, tray)) return true;
                            break;
                        default:
                            if (AcceleratorCommon::intersectAny(surface->shape, surface->material->opacity, tray)) return true;
                    }

                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->secondChild;
                } else {
                    stack[sptr++] = node->secondChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return false;
}


void BvhAccelerator::clearAccelerator() {
    for (int i = 0; i < surfaces.size(); i++) {
        if (surfaces[i]->aux) {
            SurfaceBvh* bvh = (SurfaceBvh*)surfaces[i]->aux;
            delete bvh;
        }
        delete surfaces[i];
    }
    surfaces.clear();
}

jsonObject BvhAccelerator::statistics() const {
    jsonObject json;
    json["sufaces"] = (int)surfaces.size();
    json["bbox-max"] = _worldBound.max;
    json["bbox-min"] = _worldBound.min;
    json["object-nodes"] = _stat.objectNodeNum;
    json["surface-nodes"] = _stat.surfaceNodeNum;
    json["total_memory(mb)"] = _stat.bytes / space_consts<int>::mega;
    return json;
}
