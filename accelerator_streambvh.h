#ifndef ACCELERATOR_STREAMBVH_H
#define ACCELERATOR_STREAMBVH_H

#include "accelerator.h"
#include <tbb/atomic.h>
#include <functional>
#include <tbb/mutex.h>
#include <scene/shape.h>
#include "accelerator_list.h"
#include <common/progress.h>

enum FTYPE { VPOS = 1, VNORM = 2, VUV = 4, VPOSNORM = 3, VPOSUV = 5, VALL = 7 };

struct Vertex {
    Vertex() = default;
    Vertex(const vec3f &p) : pos(p) {}
    Vertex(const vec3f &p, const vec3f &n) : pos(p), norm(n) {}
    Vertex(const vec3f &p, const vec2f &u) : pos(p), uv(u) {}
    Vertex(const vec3f &p, const vec3f &n, const vec2f &u) : pos(p), norm(n), uv(u) {}
    vec3f   pos;
    vec3f   norm;
    vec2f   uv;
};

enum { LEAFNODE = -1, BRICKNODE = -2 };

struct BrickInfo {
    uint8_t nodenum;
    uint8_t trinum;
    uint8_t vertnum;
    uint8_t lodnum;
};

struct SNode {
    SNode() = default;
    SNode(const range3f &bbox, uint8_t off, uint8_t cnt) : bound(bbox), type(LEAFNODE), offset(off), count(cnt) {}
    SNode(const range3f &bbox, int8_t ax, uint8_t l, uint8_t r) : bound(bbox), axis(ax), left(l), right(r) {}
    SNode(const range3f &bbox, uint16_t l) : bound(bbox), type(BRICKNODE), lod(l) {};
    range3f     bound;
    union { int8_t type; int8_t axis; };
    union {
        struct { uint8_t offset; uint8_t count; };
        struct { uint8_t left;   uint8_t right; };
        uint16_t lod;
    };
};

struct STriangle {
    STriangle() = default;
    STriangle(const vec3<uint8_t> &f, uint16_t m, uint8_t t)
    : face(f), mindex(m), ftype(t) {}
    vec3<uint8_t>   face;
    uint16_t        mindex : 13;
    uint8_t         ftype : 3;
};

struct Lod {
    Lod(const Vertex &vert, uint midx, float op, uint64_t br)
    : vertex(vert), mindex(midx), opacity(op), brick(br) {}
    Vertex      vertex;
    uint        mindex;
    float       opacity;
    uint64_t    brick;
};

typedef std::tuple<const SNode*, const STriangle*, const Vertex*, const Lod*> ConstBrick;
typedef std::tuple<SNode*, STriangle*, Vertex*, Lod*> Brick;
inline ConstBrick getArrays(char *buffer) {
    BrickInfo info = *((BrickInfo*)buffer);
    buffer += sizeof(BrickInfo);
    SNode* snodes = (SNode*)(buffer);
    buffer += sizeof(SNode) * info.nodenum;
    STriangle* striangles = (STriangle*)(buffer);
    buffer += sizeof(STriangle) * info.trinum;
    Vertex* vertices = (Vertex*)(buffer);
    buffer += sizeof(Vertex) * info.vertnum;
    Lod* lods = (Lod*)(buffer);
    return ConstBrick { snodes, striangles, vertices, lods };
}

inline BrickInfo getBrickInfo(char *buffer) {
    BrickInfo info = *((BrickInfo*)buffer);
    return info;
}

inline Brick getPointers(const BrickInfo &info, char *buffer) {
    *((BrickInfo*)buffer) = info;
    buffer += sizeof(BrickInfo);
    SNode* snodes = (SNode*)(buffer);
    buffer += sizeof(SNode) * info.nodenum;
    STriangle* striangles = (STriangle*)(buffer);
    buffer += sizeof(STriangle) * info.trinum;
    Vertex* vertices = (Vertex*)(buffer);
    buffer += sizeof(Vertex) * info.vertnum;
    Lod* lods = (Lod*)(buffer);
    return Brick { snodes, striangles, vertices, lods };
}

inline uint getBrickSize(const BrickInfo &info) {
    return sizeof(BrickInfo) +
    sizeof(SNode) * info.nodenum +
    sizeof(STriangle) * info.trinum +
    sizeof(Vertex) * info.vertnum +
    sizeof(Lod) * info.lodnum;
}

template<typename R>
struct intersectionlod3 {
    intersectionlod3() : m(0), sceneobject(0) {}
    R t;
    R mint;
    frame3<R> f;
    vec2<R> st;
    const Material* m;
    const void* sceneobject;
};

struct StreamBvhAcceleratorOpts : AcceleratorOpts {
    StreamBvhAcceleratorOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    std::string         treefilename = "streambvhaccelerator.tree";
};


typedef intersectionlod3<float> intersectionlod3f;
class StreamBvhAccelerator : public Accelerator {
public:
    static const int TYPEID = 30012;
    StreamBvhAccelerator();
    StreamBvhAccelerator(const StreamBvhAcceleratorOpts &opts);
    virtual ~StreamBvhAccelerator();
    virtual bool buildAccelerator(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights);
    virtual void clearAccelerator();

    virtual bool intersectFirst(const ray3f& ray, intersection3f& intersection) const;
    virtual bool intersectAny(const ray3f& ray) const;

    virtual bool    intersectFirstLOD(const ray3f& ray, intersectionlod3f& intersection, float sigma2);
    virtual float   intersectAnyLOD(const ray3f& ray, float sigma2) const;
    virtual range3f computeWorldBound() const { return _worldBound; }
    
    virtual jsonObject  statistics() const;
private:
    bool    loadBvhFromFile();
    void    listMaterials(std::vector<Surface*> &surfaces);

    StreamBvhAcceleratorOpts                _opts;
    range3f                                 _worldBound = invalidrange3f;
    std::vector<Material*>                  materials;
    ListAccelerator                         *listAccelerator = 0;
    int                                     fd = 0;
    size_t                                  fileLen = 0;
    char*                                   brickBuffer = 0;
    uint64_t                                rootBrick = 0;
};





#endif
