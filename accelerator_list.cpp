#include "accelerator_list.h"
#include <scene/transform.h>
#include <scene/shape.h>

ListAcceleratorOpts::ListAcceleratorOpts() : AcceleratorOpts(ListAccelerator::TYPEID) {}

jsonObject ListAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "listacceleratoropts";
    return json;
}

void ListAcceleratorOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("listacceleratoropts");
}

ListAccelerator::ListAccelerator() : Accelerator(TYPEID) {}

ListAccelerator::ListAccelerator(const ListAcceleratorOpts &opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

bool ListAccelerator::buildAccelerator(const std::vector<Surface*>& surfaces,
                                       const std::vector<Light*>& lights) {
    this->_surfaces = AcceleratorCommon::buildSurfaces(surfaces, lights, true);
    this->_worldBound = AcceleratorCommon::computeWorldBound(this->_surfaces);
    return true;
}

void ListAccelerator::clearAccelerator() {
    for(int i = 0; i < _surfaces.size(); i ++) {
        delete _surfaces[i];
    }
    _surfaces.clear();
}

bool ListAccelerator::intersectFirst(const ray3f& ray,
                                     sintersection3f& intersection, AcceleratorSurface*& surface) const {
    if (!intersectBoundingBox(_worldBound, ray))
        return false;

    float maxt = ray.maxt; bool hit = false;
    for(int i = 0; i < _surfaces.size(); i ++) {
        AcceleratorSurface* asurface = _surfaces[i];
        ray3f tray = transformRayInverse(asurface->transform, ray);
        tray.maxt = maxt;
        sintersection3f si;
        bool sh = AcceleratorCommon::intersectFirst(asurface->shape, asurface->material->opacity, tray, si);
        if(sh && (si.t < intersection.t || !hit)) {
            hit = true;
            intersection = si;
            surface = asurface;
            maxt = intersection.t;
        }
    }
    return hit;
}

bool ListAccelerator::intersectFirst(const ray3f& ray, intersection3f& intersection) const {
    sintersection3f si; AcceleratorSurface* surface;
    bool hit = intersectFirst(ray, si, surface);
    if(hit) AcceleratorCommon::intersectResolve(si, surface, ray, intersection);
    return hit;
}

bool ListAccelerator::intersectAny(const ray3f& ray) const {
    if (!intersectBoundingBox(_worldBound, ray))
        return false;

    for(int i = 0; i < _surfaces.size(); i ++) {
        AcceleratorSurface* asurface = _surfaces[i];
        ray3f tray = transformRayInverse(asurface->transform,ray);
        if(AcceleratorCommon::intersectAny(asurface->shape, asurface->material->opacity, tray))
            return true;
    }
    return false;
}

range3f ListAccelerator::computeWorldBound() const {
    return AcceleratorCommon::computeWorldBound(_surfaces);
}

jsonObject ListAccelerator::statistics() const {
    jsonObject json;
    json["sufaces"] = (int)_surfaces.size();
    json["bbox_max"] = _worldBound.max;
    json["bbox_min"] = _worldBound.min;
    return json;
}

