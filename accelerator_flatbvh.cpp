#include "accelerator_flatbvh.h"
#include <scene/transform.h>

using namespace std;
using namespace FlatBvh;

FlatBvhAcceleratorOpts::FlatBvhAcceleratorOpts() : AcceleratorOpts(FlatBvhAccelerator::TYPEID) {}

jsonObject FlatBvhAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "flatbvhacceleratoropts";
    json["maxleafitems"] = maxLeafItems;
    return json;
}

void FlatBvhAcceleratorOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("flatbvhacceleratoropts");
    maxLeafItems = jsonGet(json, "maxleafitems");
}

const int TRAVERSAL_STACK_SIZE = 64;

FlatBvhAccelerator::FlatBvhAccelerator(): Accelerator(TYPEID) {}

FlatBvhAccelerator::FlatBvhAccelerator(const FlatBvhAcceleratorOpts& opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

bool FlatBvhAccelerator::buildAccelerator(const std::vector<Surface*>& surfs,
                                          const std::vector<Light*>& lights) {
    this->surfaces = AcceleratorCommon::buildSurfaces(surfs, decltype(lights){}, true);
    if (this->surfaces.size() == 0) {
        _worldBound = range3f(zero3f, zero3f);
        return true;
    } else {
        buildFlatBvhAccelerator();
        return true;
    }
}

void FlatBvhAccelerator::clearAccelerator() {
    for_each(surfaces.begin(), surfaces.end(), [](const AcceleratorSurface *s)->void {
        delete s;
    });
    surfaces.clear();
}

FlatBvhAccelerator::~FlatBvhAccelerator() { clearAccelerator(); }

bool FlatBvhAccelerator::intersectFirst(const ray3f& ray, sintersection3f& intersection,
                                        AcceleratorSurface*& surface) const {
    std::array<int, TRAVERSAL_STACK_SIZE> stack;
    bool hit = false;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    int nodeNum = 0;
    while (true) {
        const CompactBvhNode *node = nodes.data() + nodeNum;
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) { // Leaf Node
                for (int s = 0; s < node->nitems; s++) {
                    int index = node->offset + s;
                    const vec3i &f = faces[index];
                    float t; vec2f uv;
                    const vec3f &v0 = vertices[f.x];
                    const vec3f &v1 = vertices[f.y];
                    const vec3f &v2 = vertices[f.z];
                    bool th = intersectTriangle(v0, v1, v2, ray, t, uv);
                    if (th && (!hit || intersection.t > t)) {
                        hit = true;
                        surface = surfaces[meshIds[index]];
                        vec3f hp = transformPointInverse(surface->transform, ray.eval(t));
                        intersection = { .t = t, .p = hp, .uv = uv, .idx = triangleIds[index] };
                    }
                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->rightChild;
                } else {
                    stack[sptr++] = node->rightChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return hit;
}

bool FlatBvhAccelerator::intersectFirst(const ray3f& ray, intersection3f& intersection) const {
    sintersection3f si; AcceleratorSurface* surface;
    bool hit = intersectFirst(ray, si, surface);
    if(hit) AcceleratorCommon::intersectResolve(si, surface, ray, intersection);
    return hit;
}

bool FlatBvhAccelerator::intersectAny(const ray3f& ray) const {
    int nodeNum = 0;
    std::array<int, TRAVERSAL_STACK_SIZE> stack;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    while (true) {
        const CompactBvhNode *node = nodes.data() + nodeNum;
        if (intersectBoundingBox(node->bbox, ray, invDir)) {
            if (node->axis == -1) { // Leaf Node
                for (int s = 0; s < node->nitems ; s++) {
                    int index = node->offset + s;
                    const vec3i &f = faces[index];
                    float t; vec2f uv;
                    const vec3f &v0 = vertices[f.x];
                    const vec3f &v1 = vertices[f.y];
                    const vec3f &v2 = vertices[f.z];
                    bool th = intersectTriangle(v0, v1, v2, ray, t, uv);
                    if (th) return true;
                }
                if (sptr <= 0) break;
                nodeNum = stack[--sptr];
            } else {
                if (invDir[node->axis] < 0) {
                    stack[sptr++] = nodeNum + 1;
                    nodeNum = node->rightChild;
                } else {
                    stack[sptr++] = node->rightChild;
                    nodeNum = nodeNum + 1;
                }
            }
        } else {
            if (sptr <= 0) break;
            nodeNum = stack[--sptr];
        }
    }
    return false;
}

jsonObject FlatBvhAccelerator::statistics() const {
    jsonObject json;
    json["sufaces"] = (int)surfaces.size();
    json["bbox-max"] = _worldBound.max;
    json["bbox-min"] = _worldBound.min;
    json["node-number"] = (int)nodes.size();
    json["triangle-number"] = (int)faces.size();
    return json;
}