#ifndef ACCELERATOR_BVH_H
#define ACCELERATOR_BVH_H

#include "accelerator.h"

namespace BVH {
struct LinearBvhNode {
    range3f bbox;
    union {
        int offset;
        int secondChild;
    };
    int16_t  axis;
    uint16_t nitems;
};

struct SurfaceBvh : AcceleratorAux {
    std::vector<LinearBvhNode>  nodes;
    std::vector<int>            indices;
};
}

enum BvhSplitMode { SPLIT_MIDDLE, SPLIT_EVEN, SPLIT_SAH };
struct BvhAcceleratorOpts : AcceleratorOpts {
    BvhAcceleratorOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxLeafItems = 16;
    BvhSplitMode        surfaceSplitMode = SPLIT_SAH;
    BvhSplitMode        shapeSplitMode = SPLIT_SAH;
    bool                parallel = true;
};

struct BvhAcceleratorStat {
    BvhAcceleratorStat() : objectNodeNum(0), surfaceNodeNum(0), bytes(0) {}
    uint64_t objectNodeNum;
    uint64_t surfaceNodeNum;
    uint64_t bytes;
};

class BvhAccelerator : public Accelerator {
public:
    static const int TYPEID = 30002;
    BvhAccelerator();
    BvhAccelerator(const BvhAcceleratorOpts &opts);
    virtual ~BvhAccelerator();
    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces,
                                         const std::vector<Light*>& lights);
    virtual void        clearAccelerator();
    virtual bool        intersectFirst(const ray3f& ray, sintersection3f& intersection,
                                       AcceleratorSurface*& surface) const;
    virtual bool        intersectFirst(const ray3f& ray, intersection3f& intersection) const;
    virtual bool        intersectAny(const ray3f& ray) const;
    virtual range3f     computeWorldBound() const { return _worldBound; }
    virtual jsonObject  statistics() const;

protected:
    void                buildBvhAccelerator();

    std::vector<AcceleratorSurface*>    surfaces;
    std::vector<BVH::LinearBvhNode>     nodes;
    range3f                             _worldBound;
    BvhAcceleratorOpts                  _opts;
    BvhAcceleratorStat                  _stat;
};





#endif
