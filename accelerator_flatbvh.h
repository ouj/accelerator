#ifndef ACCELERATOR_FLAT_BVH_H
#define ACCELERATOR_FLAT_BVH_H

#include "accelerator.h"

struct FlatBvhAcceleratorOpts : AcceleratorOpts {
    FlatBvhAcceleratorOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
    int                 maxLeafItems = 16;
};

namespace FlatBvh {
struct CompactBvhNode {
    range3f             bbox;
    int8_t              axis;
    union {
        struct {
            uint8_t     nitems;
            uint32_t    offset;
        };
        uint32_t        rightChild;
    };
};
}

class FlatBvhAccelerator : public Accelerator {
public:
    static const int TYPEID = 300013;
    FlatBvhAccelerator();
    FlatBvhAccelerator(const FlatBvhAcceleratorOpts &opts);
    virtual ~FlatBvhAccelerator();
    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces,
                                         const std::vector<Light*>& lights);
    virtual void        clearAccelerator();
    virtual bool        intersectFirst(const ray3f& ray, sintersection3f& intersection,
                                       AcceleratorSurface*& surface) const;
    virtual bool        intersectFirst(const ray3f& ray, intersection3f& intersection) const;
    virtual bool        intersectAny(const ray3f& ray) const;
    virtual range3f     computeWorldBound() const { return _worldBound; }
    virtual jsonObject  statistics() const;

private:
    void buildFlatBvhAccelerator();
    std::vector<FlatBvh::CompactBvhNode>        nodes;
    std::vector<vec3i>                          faces;
    std::vector<vec3f>                          vertices;
    std::vector<int>                            meshIds;
    std::vector<int>                            triangleIds;
    std::vector<AcceleratorSurface*>            surfaces;
    range3f                                     _worldBound;
    FlatBvhAcceleratorOpts                      _opts;

    friend class GpuBvhAccelerator;
};



#endif