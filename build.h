#ifndef ACCELERATORIO_H
#define ACCELERATORIO_H
#include <common/json.h>
#include "accelerator.h"

AcceleratorOpts* parseAcceleratorOpts(const jsonObject &json);

Accelerator* createAccelerator(const AcceleratorOpts *opts);
Accelerator* createAccelerator(const jsonObject &json);

Accelerator* createDefaultAccelerator();

#endif
