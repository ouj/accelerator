#include "accelerator_streambvh_build.h"

#include <tbb/tbb.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <common/stdheader.h>
#include <common/stddir.h>
#include <common/distribution.h>
#include <common/progress.h>
#include <common/raw.h>
#include <common/binio.h>
#include <common/morton.h>
#include <common/stddir.h>
#include <common/lru.h>

#include <scene/transform.h>
#include <scene/shape.h>
#include <scene/sceneio.h>
#include <scene/material.h>
#include <scene/scenestat.h>

using namespace std;
using namespace tbb;

enum { HASFACE = 1, HASPOS = 2, HASNORM = 4, HASUV = 8 };
bool StreamSurface::save() const {
    FILE *f = fopen(filename.c_str(), "wb");
    if (f == 0) return false;
    int flag = 0;
    if (!face.empty()) flag |= HASFACE;
    if (!pos.empty()) flag |= HASPOS;
    if (!norm.empty()) flag |= HASNORM;
    if (!uv.empty()) flag |= HASUV;
    fwrite(&flag, sizeof(int), 1, f);
    if (!face.empty()) writeRaw(f, face);
    if (!pos.empty()) writeRaw(f, pos);
    if (!norm.empty()) writeRaw(f, norm);
    if (!uv.empty()) writeRaw(f, uv);
    fclose(f);
    return true;
}

bool StreamSurface::load() {
    FILE *f = fopen(filename.c_str(), "rb");
    if (f == 0) return false;
    int flag = 0;
    error_if_not(fread(&flag, sizeof(int), 1, f) == sizeof(int), "incorrect read");
    if (flag & HASFACE) readRaw(f, face);
    if (flag & HASPOS)  readRaw(f, pos);
    if (flag & HASNORM) readRaw(f, norm);
    if (flag & HASUV)   readRaw(f, uv);
    fclose(f);
    return true;
}

void StreamSurface::unload() {
    face.clear(); face.shrink_to_fit();
    pos.clear(); pos.shrink_to_fit();
    norm.clear(); norm.shrink_to_fit();
    uv.clear(); uv.shrink_to_fit();
}

struct SAHBucket {
    SAHBucket() : count(0), bbox(invalidrange3f){}
    int			count;
    range3f     bbox;
};

struct Bucket {
    vec3i                       gindex;
    unsigned int                code;
    range3f                     bound; //triangle centroid bound
    unsigned int                count;
};

struct Subregion {
    Subregion() = default;
    Subregion(const range3f &b, int c) : bound(b), count(c) {}
    void                    merge(StreamSurface *ssurface);
    range3f                 bound;
    int                     count = 0;
    vector<StreamSurface*>  surfaces;
};

struct SubregionNode {
    SubregionNode() = default;
    SubregionNode(int8_t a, float s, int rg, SubregionNode* l, SubregionNode *r)
        : axis(a), split(s), region(rg), left(l), right(r) {}
    ~SubregionNode() { if (left) delete left; if (right) delete right; }
    int8_t axis = -1;
    float  split = 0;
    int    region = -1;
    SubregionNode *left = 0;
    SubregionNode *right = 0;
};

inline float bboxArea(const range3f &b) {
    vec3f d = b.extent();
    return 2.f * (d.x * d.y + d.x * d.z + d.y * d.z);
}

SubregionNode* rbucketPartition(std::vector<Bucket>::iterator begin, std::vector<Bucket>::iterator end,
                                const vec3f &origin, float interval,
                                std::vector<Subregion> &regions, int chunkSize) {
    range3i bound = invalidrange3i;
    int count = 0;
    for (auto it = begin; it != end; ++it) {
        bound.grow(it->gindex);
        count += it->count;
    }

    int8_t dim = bound.extent().max_component_index();
    if (count <= chunkSize || bound.max[dim] == bound.min[dim]) {
        SubregionNode *node = new SubregionNode { -1, 0, (int)regions.size(), 0, 0 };
        vec3f fmin(bound.min.x, bound.min.y, bound.min.z);
        vec3f fmax(bound.max.x + 1, bound.max.y + 1, bound.max.z + 1);
        range3f frange(origin + fmin * interval,
                       origin + fmax * interval);
        regions.emplace_back(frange, count);
        return node;
    }
    int range = bound.max[dim] - bound.min[dim] + 1;
    error_if(range <= 1, "less then two");
    vector<int> slices(range, 0); //slice triangle count

    for (auto it = begin; it != end; ++it) {
        int index = it->gindex[dim] - bound.min[dim];
        slices[index] += it->count;
    }

    int half = count * 0.5f;
    int lcount = slices[0]; int si;
    for (si = 1; si < slices.size() - 1; si++) {
        if (lcount + slices[si] > half) break;
        lcount += slices[si];
    }
    int split = bound.min[dim] + si;
    auto mid = std::partition(begin, end, [split, dim](const Bucket& b) { return b.gindex[dim] < split; });
    error_if_va(mid == begin || mid == end, "incorrect split %d %d", end - mid, mid - begin);
    float fsplit = origin[dim] + split * interval;

    auto left = rbucketPartition(begin, mid, origin, interval, regions, chunkSize);
    auto right = rbucketPartition(mid, end, origin, interval, regions, chunkSize);
    SubregionNode *node = new SubregionNode { dim, fsplit, -1, left, right };
    return node;
}

vec3i gridIndex(const vec3f &pt, const vec3f &origin, float interval) {
    vec3f i = floor_component((pt - origin) / interval);
    return vec3i((int)i.x, (int)i.y, (int)i.z);
}

int findRegion(vec3f &center, SubregionNode* node) {
    if (node->axis == -1) { return node->region; }
    else if (center[node->axis] < node->split) return findRegion(center, node->left);
    else return findRegion(center, node->right);
}

static StreamSurface *convert(const Surface *surface, const string &folderName) {
    if (surface->shape->type != TriangleMeshShape::TYPEID) return 0;
    StreamSurface *ssurface = new StreamSurface();
    TriangleMeshShape *shape = (TriangleMeshShape*)surface->shape;
    Transform* transform = surface->transform;
    Material* material = surface->material;
    load(shape);
    ssurface->uv = shape->uv;
    ssurface->face = shape->face;
    ssurface->pos.resize(shape->pos.size());
    ssurface->norm.resize(shape->norm.size());
    for (int i = 0; i < shape->pos.size(); i++) {
        ssurface->pos[i] = transformPoint(transform, shape->pos[i]);
        if (!shape->norm.empty())
            ssurface->norm[i] = transformNormal(transform, shape->norm[i]);
    }
    unload(shape);
    ssurface->filename = folderName + surface->name;
    ssurface->material = material;
    return ssurface;
}

void split(Surface* surface, vector<Subregion> &regions, SubregionNode* root, const string &folderName) {
    if(surface->shape->type != TriangleMeshShape::TYPEID) return;
    unordered_map<int, vector<vec3i>> trianglesList;
    TriangleMeshShape *ss = (TriangleMeshShape*)surface->shape;
    load(ss);
    for(int i = 0; i < ss->face.size(); i++) {
        const vec3i &f = ss->face[i];
        vec3f v0 = transformPoint(surface->transform, ss->pos[f.x]);
        vec3f v1 = transformPoint(surface->transform, ss->pos[f.y]);
        vec3f v2 = transformPoint(surface->transform, ss->pos[f.z]);
        vec3f center = triangleCenter(v0, v1, v2);
        int region = findRegion(center, root);
        trianglesList[region].push_back(f);
    }

    for (auto &p : trianglesList) {
        int regIndex = p.first;
        error_if(regIndex < 0 || regIndex >= regions.size(), "invalid regIndex");
        Subregion &reg = regions[regIndex];
        const vector<vec3i> &face = p.second;
        unordered_map<int, int> verts;
        int k = 0;
        for (auto &f : face) {
            for (int i = 0; i < 3; i++) {
                auto ret = verts.insert({f[i], k});
                if (ret.second) ++k;
            }
        }
        StreamSurface *ns = new StreamSurface();
        ns->filename = folderName + surface->name + "-" +  to_string(regIndex);
        ns->material = surface->material;
        ns->face.reserve(face.size());
        for (auto &f : face) {
            ns->face.push_back(vec3i{verts[f.x], verts[f.y], verts[f.z]});
        }
        if (!ss->pos.empty()) ns->pos.resize(verts.size());
        if (!ss->norm.empty()) ns->norm.resize(verts.size());
        if (!ss->uv.empty()) ns->uv.resize(verts.size());
        for (auto &v : verts) {
            ns->pos[v.second] = transformPoint(surface->transform, ss->pos[v.first]);
            if (!ss->norm.empty()) ns->norm[v.second] = transformNormal(surface->transform, ss->norm[v.first]);
            if (!ss->uv.empty()) ns->uv[v.second] = ss->uv[v.first];
        }
        reg.surfaces.push_back(ns);
        ns->save();
        ns->unload();
    }
    unload(ss);
}

static void chunking(Surface* surface, const range3f &bbox,
                     std::vector<Subregion> &regions, SubregionNode* node, const string &folderName) {
    if (node->axis == -1) {
        Subregion &reg = regions[node->region];
        StreamSurface *ssurface = convert(surface, folderName);
        reg.surfaces.push_back(ssurface);
        ssurface->save();
        ssurface->unload();
        return;
    }
    if(bbox.max[node->axis] < node->split) ::chunking(surface, bbox, regions, node->left, folderName);
    else if (bbox.min[node->axis] > node->split) ::chunking(surface, bbox, regions, node->right, folderName);
    else ::split(surface, regions, node, folderName);
}


void StreamAcceleratorBuilder::chunking(vector<Surface*> &surfaces, vector<range3f> &sbounds,
                                        vector<Bucket> &buckets, vector<Subregion> &regions,
                                        vec3f &origin, float interval) const {
    ps.start("Chunking");
    SubregionNode* root = 0;
    ps.fn_single("Partition Buckets", [&]() {
        root = ::rbucketPartition(buckets.begin(), buckets.end(),
                                  origin, interval, regions,
                                  options::streamaccelerator::chuncksize);
    });

    ps.progress_start("Surface Streaming");
    string folderPrefix = sceneName + "-flatten/";
    mkdir(folderPrefix.c_str(), 0777);
    for(int s = 0; s < surfaces.size(); s++) {
        ::chunking(surfaces[s], sbounds[s], regions, root, folderPrefix);
        ps.progress_update((float)(s+1) / surfaces.size());
    }
    ps.progress_end();
    if (root) delete root;
    ps.end();
}


tuple<range3f, uint64_t, uint64_t> computerWorldBound(vector<Surface*> &surfaces,
                                                      std::vector<range3f> &sbounds,
                                                      Progress &ps) {
    ps.progress_start("Compute world bound");
    range3f worldBound = invalidrange3f;
    uint64_t count = 0;
    uint64_t bytes = 0;
    for (int s = 0; s < surfaces.size(); s++) {
        Surface *ss = surfaces[s];
        range3f &bound = sbounds[s];
        bound = invalidrange3f;
        if(ss->shape->type != TriangleMeshShape::TYPEID) continue;
        TriangleMeshShape *shape = (TriangleMeshShape*)ss->shape;
        load(shape);
        for (auto &f : shape->face) {
            vec3f v0 = transformPoint(ss->transform, shape->pos[f.x]);
            vec3f v1 = transformPoint(ss->transform, shape->pos[f.y]);
            vec3f v2 = transformPoint(ss->transform, shape->pos[f.z]);
            vec3f center = triangleCenter(v0, v1, v2);
            bound.grow(center);
        }
        count += shape->face.size();
        bytes += getMemoryUsage(shape);
        worldBound.grow(bound);
        unload(shape);
        ps.progress_update((float)(s+1) / surfaces.size());
    }
    ps.progress_end();
    return tuple<range3f, uint64_t, uint64_t> { worldBound, count, bytes };
}


struct BuildItem {
    BuildItem(const range3f &b, const vec3f &c, int i, float a)
    : bbox(b), centroid(c), index(i), area(a) {}
    range3f         bbox;
    vec3f           centroid;
    int             index;
    float           area;
};


struct BTriangle {
    BTriangle(const vec3i &f, uint16_t midx, FTYPE ftype)
    : f(f), mindex(midx), ftype(ftype) {}
    vec3i       f;
    uint16_t    mindex;
    char        ftype;
};

struct SLod {
    Vertex vertex;
    uint   mindex;
};

struct SBvhNode {
    SBvhNode(const range3f &b, float ar, const SLod &l)
            : bbox(b), area(ar), lod(l) {};
    virtual             ~SBvhNode() {};
    virtual bool        isLeaf() = 0;
    virtual bool        isBrick() = 0;
    range3f             bbox;
    float               area;
    SLod                lod;
};

struct SBvhBranch : SBvhNode {
    SBvhBranch(const range3f &b, float ar, int8_t a, SBvhNode *l, SBvhNode *r, const SLod &lod)
    : SBvhNode(b, ar, lod), axis(a), left(l), right(r) {}
    virtual         ~SBvhBranch() { if (left) delete left; if (right) delete right; };
    virtual bool    isLeaf() { return false; }
    virtual bool    isBrick() { return false; }
    int8_t          axis;
    SBvhNode        *left;
    SBvhNode        *right;
};

struct SBvhLeaf : SBvhNode {
    SBvhLeaf(const range3f &b, float ar, uint o, uint16_t c, const SLod &l)
    : SBvhNode(b, ar, l), offset(o), count(c) {}
    virtual bool    isLeaf() { return true; }
    virtual bool    isBrick() { return false; }
    uint            offset;
    uint16_t        count;
};

struct SBvhBrick : SBvhNode {
    SBvhBrick(range3f b, float ar, uint64_t br, const SLod &l)
    : SBvhNode(b, ar, l), brick(br) {}
    virtual bool    isLeaf() { return false; }
    virtual bool    isBrick() { return true; }
    uint64_t        brick;
};

SBvhNode* copyBVH(SBvhNode* node) {
    if (node->isLeaf()) {
        SBvhLeaf *leaf = (SBvhLeaf*)node;
        return new SBvhLeaf(*leaf);
    } else if (node->isBrick()) {
        SBvhBrick *brick = (SBvhBrick*)node;
        return new SBvhBrick(*brick);
    } else {
        SBvhBranch *branch = (SBvhBranch*)node;
        SBvhBranch *nbranch = new SBvhBranch(*branch);
        nbranch->left = copyBVH(branch->left);
        nbranch->right = copyBVH(branch->right);
        return nbranch;
    }
}

struct Package {
    Package() = default;
    Package(const Package &p) : verts(p.verts) {
        faces = p.faces;
        nodes = p.nodes;
        lods = p.lods;
    }
    Package(Package &&p) noexcept : verts(std::move(p.verts)) {
        faces = p.faces;
        nodes = p.nodes;
        lods = p.lods;
    }
    unordered_map<int, uint8_t> verts;
    int                         faces = 0;
    int                         nodes = 0;
    int                         lods = 0;
};

struct TreeSerializer {
    TreeSerializer() = default;
    ~TreeSerializer();
    bool                    init(const string &bvhfilename);
    SBvhBrick*              process(SBvhNode *root, const vector<uint> *st, const vector<BTriangle> *t,
                                    const vector<pair<StreamSurface*, uint>> *v);
    void                    writeRootBrick(uint64_t rbrick);
    int                     fd = 0;
    tbb::atomic<off_t>      fileOff;
};

TreeSerializer::~TreeSerializer() {
//    if (bufOff) flushIO();
//    if (buffer) delete [] buffer;
    if (fd) close(fd);
}

bool TreeSerializer::init(const string &bvhfilename) {
    fd = open(bvhfilename.c_str(), O_WRONLY|O_CREAT, 0666);
    if(fd < 0) { error("failed to create tree file"); return false; }
    fileOff = sizeof(uint64_t); // save for root brick id;
    return true;
}

void TreeSerializer::writeRootBrick(uint64_t rbrick) {
    int written = pwrite(fd, &rbrick, sizeof(uint64_t), 0); //write to the beginning
    warning_if_va(written != sizeof(uint64_t), "incorrect written bytes");
}

template<typename P>
Package treePartition(SBvhNode *node, const vector<uint> *sorted, const vector<BTriangle> *tris,
                      const vector<pair<StreamSurface*, uint>> *verts, P& process) {
    if (node->isLeaf()) {
        Package pack;
        SBvhLeaf *leaf = (SBvhLeaf*)node;
        for(int i = 0; i < leaf->count; i++) {
            int f = sorted->at(i + leaf->offset);
            pack.faces++;
            pack.verts.insert({tris->at(f).f.x, pack.verts.size()});
            pack.verts.insert({tris->at(f).f.y, pack.verts.size()});
            pack.verts.insert({tris->at(f).f.z, pack.verts.size()});
        }
        pack.nodes++;
        return pack;
    } else if (node->isBrick()) {
        Package pack;
        pack.nodes = 1; pack.lods = 1; pack.faces = 0;
        return pack;
    } else {
        Package pack;
        SBvhBranch *branch = (SBvhBranch*)node;
        auto lpack = treePartition(branch->left, sorted, tris, verts, process);
        auto rpack = treePartition(branch->right, sorted, tris, verts, process);
        pack.verts = lpack.verts;
        for_each(rpack.verts.begin(), rpack.verts.end(),
                 [&pack](map<int, uint8_t>::value_type &p){
                     pack.verts.insert({p.first, pack.verts.size()});
                 });
        pack.faces = lpack.faces + rpack.faces;
        pack.nodes = lpack.nodes + rpack.nodes + 1;
        pack.lods = lpack.lods + rpack.lods;
        constexpr int maxp = options::streamaccelerator::maxbrickprimitives;
        if(pack.faces >= maxp || pack.nodes >= maxp || pack.verts.size() >= maxp) {
            return process(branch, lpack, rpack);
        } else return pack;
    }
}


SBvhBrick* serialize(SBvhNode *root, Package& package,
                     const vector<uint> *sorted, const vector<BTriangle> *tris,
                     const vector<pair<StreamSurface*, uint>> *verts,
                     char* buffer, uint &bufOff, off_t fileOff) {
    BrickInfo info { (uint8_t)package.nodes, (uint8_t)package.faces,
        (uint8_t)package.verts.size(), (uint8_t) package.lods };
    uint brickSize = getBrickSize(info);
    SNode* snodes; STriangle* striangles; Vertex* vertices; Lod* lods;
    std::tie(snodes, striangles, vertices, lods) = getPointers(info, buffer + bufOff);

    StreamSurface *ss; int vi;
    for(auto& p : package.verts) {
        std::tie(ss, vi) = (*verts)[p.first];
        auto &sv = vertices[p.second];
        sv.pos = ss->pos[vi];
        if(!ss->norm.empty()) { sv.norm = ss->norm[vi]; }
        if(!ss->uv.empty()) { sv.uv = ss->uv[vi]; }
    };

    int faceIdx = 0;
    int snodeIdx = 0;
    int lodIdx = 0;
    queue<SBvhNode*> q;
    q.push(root);
    while (!q.empty()) {
        auto node = q.front(); q.pop();
        if(node->isLeaf()) {
            SBvhLeaf *leaf = (SBvhLeaf*)node;
            snodes[snodeIdx++] = SNode { node->bbox, (uint8_t)faceIdx, (uint8_t)leaf->count }; // create node
            for(int i = 0; i < leaf->count; i++) {
                int index = (*sorted)[i + leaf->offset];
                auto &tri = tris->at(index);
                const vec3i &f = tri.f;
                STriangle &stris = striangles[faceIdx++];
                uint8_t v0 = package.verts.at(f.x);
                uint8_t v1 = package.verts.at(f.y);
                uint8_t v2 = package.verts.at(f.z);
                //error_if_not_va(v0 < 256 && v1 < 256 && v2 < 256, "%d %d %d", v0, v1, v2);
                stris = STriangle { vec3<uint8_t> { v0, v1, v2 }, (uint16_t)tri.mindex, (uint8_t)tri.ftype };
            }
        } else if (node->isBrick()) {
            SBvhBrick *brick = (SBvhBrick*)node;
            snodes[snodeIdx++] = SNode { brick->bbox, (uint16_t)lodIdx };
            // compute opacity on fly
            float op = min(1.0f, brick->area / bboxArea(brick->bbox));
            lods[lodIdx++] = Lod { brick->lod.vertex, brick->lod.mindex, op, brick->brick};
        } else {
            SBvhBranch *branch = (SBvhBranch*)node;
            auto left = snodeIdx + q.size() + 1;
            auto right = snodeIdx + q.size() + 2;
            snodes[snodeIdx++] = SNode { node->bbox, branch->axis, (uint8_t)left, (uint8_t)right };
            q.push(branch->left);
            q.push(branch->right);
        }
    }
    error_if(faceIdx != package.faces || snodeIdx != package.nodes || lodIdx != package.lods,
             "incorrect item numbers");
    SBvhBrick *brick = new SBvhBrick { root->bbox, root->area, static_cast<uint64_t>(fileOff + bufOff), root->lod };
    delete root;
    bufOff += brickSize;
    return brick;
}


SBvhBrick* TreeSerializer::process(SBvhNode *root, const vector<uint> *sorted, const vector<BTriangle> *tris,
                                   const vector<pair<StreamSurface*, uint>> *verts) {
    uint bufsize = 0;
    auto computesize = [tris, verts, sorted, &bufsize, this]
                       (SBvhBranch *branch, Package &lpack, Package &rpack)->Package {
        bufsize += getBrickSize(BrickInfo { (uint8_t)lpack.nodes, (uint8_t)lpack.faces,
            (uint8_t)lpack.verts.size(), (uint8_t) lpack.lods });
        bufsize += getBrickSize(BrickInfo { (uint8_t)rpack.nodes, (uint8_t)rpack.faces,
            (uint8_t)rpack.verts.size(), (uint8_t) rpack.lods });
        Package pack;
        pack.nodes = 3; pack.faces = 0; pack.lods = 2;
        return pack;
    };

    auto p = treePartition(root, sorted, tris, verts, computesize);
    bufsize += getBrickSize(BrickInfo { (uint8_t)p.nodes, (uint8_t)p.faces,
                (uint8_t)p.verts.size(), (uint8_t) p.lods });

    off_t offset = fileOff.fetch_and_add(bufsize);
    char* buffer = new char[bufsize];
    error_if(buffer == 0, "Failed to allocate buffer");
    uint bufOff = 0;
    auto serialization = [tris, verts, buffer, sorted,
                          &bufOff, offset, this] (SBvhBranch *branch, Package &lpack, Package &rpack)->Package {
        branch->left = ::serialize(branch->left, lpack, sorted, tris, verts, buffer, bufOff, offset);
        branch->right = ::serialize(branch->right, rpack, sorted, tris, verts, buffer, bufOff, offset);
        Package pack;
        pack.nodes = 3; pack.faces = 0; pack.lods = 2;
        return pack;
    };
    Package pack = treePartition(root, sorted, tris, verts, serialization);
    SBvhBrick* brick = ::serialize(root, pack, sorted, tris, verts, buffer, bufOff, offset);

    error_if_not_va(bufsize == bufOff, "buffer size not equal %d != %d", bufsize, bufOff);
    int written = pwrite(fd, buffer, bufsize, offset);
    warning_if_va(written != bufsize, "flush io: incorrect written bytes %d!=%d", written, bufsize);

    delete [] buffer;
    return brick;
}

//#define VERIFY_BVH
#ifdef VERIFY_BVH
bool verify(uint64_t brick, uint8_t nIndex, SBvhNode *node, off_t offset, char *buffer,
            const vector<uint> *sorted, const vector<BTriangle> *tris,
            const vector<pair<StreamSurface*, uint>> *verts) {
    BrickInfo bi = getBrickInfo(buffer + (brick - offset));
//    message_va("%d - node: %d tris: %d verts: %d lod: %d", (brick - offset), bi.nodenum, bi.trinum, bi.vertnum, bi.lodnum);
    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* vertices = 0; const Lod *lods = 0;
    std::tie(snodes, striangles, vertices, lods) = getArrays(buffer + (brick - offset));

    error_if_va(nIndex >= bi.nodenum || nIndex < 0, "invalid node index %d / %d", nIndex, bi.nodenum);
    const SNode *snode = snodes + nIndex;
    while (snode->type == BRICKNODE) {
        error_if(snode->lod >= bi.lodnum || snode->lod < 0, "invalid lod index");
        const Lod &lod = lods[snode->lod];
        bi = getBrickInfo(buffer + (lod.brick - offset));
        std::tie(snodes, striangles, vertices, lods) = getArrays(buffer + (lod.brick - offset));
        brick = lod.brick;
        nIndex = 0;
        snode = snodes + nIndex;
//        message_va("jump to new brick %d", brick);
    }

    error_if_not(snode->bound.min == node->bbox.min, "incorrect bbox");
    error_if_not(snode->bound.max == node->bbox.max, "incorrect bbox");

    if (snode->type == LEAFNODE) {
        error_if_not(node->isLeaf(), "incorrect node type");
        SBvhLeaf* leaf = (SBvhLeaf*)node;
        error_if(leaf->count != snode->count, "incorrect triangle count");
        for(int i = 0; i < leaf->count; i++) {
            int fi = sorted->at(leaf->offset + i);
            int sfi = snode->offset + i;
            error_if_va(sfi >= bi.trinum || sfi < 0, "invalid triangle index %d / %d", sfi, bi.trinum);
            const STriangle &striangle = striangles[sfi];
            vec3i face = tris->at(fi).f;
            uint mindex = tris->at(fi).mindex;
            error_if(mindex != striangle.mindex, "material not match");
            StreamSurface *ss;
            int i0, i1, i2;
            std::tie(ss, i0) = verts->at(face.x);
            std::tie(ss, i1) = verts->at(face.y);
            std::tie(ss, i2) = verts->at(face.z);
            error_if_va(i0 < 0 || i0 >= ss->pos.size() ||
                        i1 < 0 || i1 >= ss->pos.size() ||
                        i2 < 0 || i2 >= ss->pos.size(), "invalid index %d - %d - %d / %d", i0, i1, i2, ss->pos.size());
            const vec3f &v0 = ss->pos.at(i0);
            const vec3f &v1 = ss->pos.at(i1);
            const vec3f &v2 = ss->pos.at(i2);
            const Vertex &sv0 = vertices[striangle.face.x];
            const Vertex &sv1 = vertices[striangle.face.y];
            const Vertex &sv2 = vertices[striangle.face.z];
            error_if_va(striangle.face.x < 0 || striangle.face.x >= bi.vertnum ||
                        striangle.face.y < 0 || striangle.face.y >= bi.vertnum ||
                        striangle.face.z < 0 || striangle.face.z >= bi.vertnum,
                        "invalid vertice index %d - %d - %d / %d", striangle.face.x, striangle.face.y, striangle.face.z, bi.vertnum);
            warning_if_va(v0 != sv0.pos, "unmatch vertex pos %s != %s", v0.print().c_str(), sv0.pos.print().c_str());
            warning_if_va(v1 != sv1.pos, "unmatch vertex pos %s != %s", v1.print().c_str(), sv1.pos.print().c_str());
            warning_if_va(v2 != sv2.pos, "unmatch vertex pos %s != %s", v2.print().c_str(), sv2.pos.print().c_str());
        }
    } else {
        error_if(node->isLeaf() || node->isBrick(), "incorrect branch type");
        SBvhBranch *branch = (SBvhBranch*)node;
        error_if_not_va(snode->axis == branch->axis, "incorrect axis %d != %d", snode->axis, branch->axis);
        verify(brick, snode->left, branch->left, offset, buffer, sorted, tris, verts);
        verify(brick, snode->right, branch->right, offset, buffer, sorted, tris, verts);
    }
    return true;
}
#endif

template<typename CLOD>
SBvhNode* sahTreeBuild(vector<BuildItem>::iterator begin, vector<BuildItem>::iterator end,
                       vector<float> &cdf, vector<uint> &triangles, CLOD &computeLOD, Rng &rng) {
    if (begin == end) { error("invalid size"); return 0; }
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    int size = end - begin;
    float area = 0; cdf[0] = 0.0f; int k = 1;
    for_each(begin, end, [&bbox, &cbbox, &area, &k, &cdf](BuildItem &p) {
        bbox.grow(p.bbox);
        cbbox.grow(p.centroid);
        area += p.area;
        cdf[k] = cdf[k-1] + p.area;
        k++;
    });

    float lr = rng.next1f();
    int index = 0;
    if (area <= 0.0f) { //pure random
        index = lr * size;
        if (index >= size) {
            index = min<int>(index, size - 1);
            message_va("lr/area/index/k/size: %f/%f/%f/%d/%d/%d", lr, area, index, k, size);
        }
    } else {
        float ar = lr * area;
        index = std::upper_bound(cdf.begin(), cdf.begin() + k, ar) - cdf.begin() - 1;
        if (index >= size) {
            message_va("lr/ar/area/index/k/size: %f/%f/%f/%d/%d/%d", lr, ar, area, index, k, size);
            index = min<int>(index, size-1);
        }
    }
    auto it = begin + index;
    auto lod = computeLOD(it->index, rng);
//    message_va("k: %d/%d", k, cdf.size());

//    message_va("index %d/%d", (ptr - cdf.begin() - 1), end - begin);

    int8_t dim = cbbox.extent().max_component_index();
    vector<BuildItem>::iterator mid;
    if (end - begin <= options::streamaccelerator::maxleaftriangles) {
        uint offset = (uint)triangles.size();
        uint16_t size = (uint16_t)(end - begin);
        for_each(begin, end, [&triangles](BuildItem &p) { triangles.push_back(p.index); });
        auto leaf = new SBvhLeaf { bbox, area, offset, size, lod };
        return leaf;
    } else if (cbbox.max[dim] == cbbox.min[dim]) {
        mid = begin + (0.5f * (end - begin));
    } else {
        constexpr int BUCKETNUM = options::streamaccelerator::sahbucketnumber;
        SAHBucket buckets[BUCKETNUM];
        float cost[options::streamaccelerator::sahbucketnumber - 1];
        float range = cbbox.max[dim] - cbbox.min[dim];
        for_each(begin, end, [&bbox, &cbbox, range, dim, &buckets](BuildItem &p) {
            float offset = p.centroid[dim] - cbbox.min[dim];
            int b = min(BUCKETNUM - 1, (int)(BUCKETNUM * (offset / range)));
            error_if(b < 0 || b >= BUCKETNUM, "invalid bucket");
            buckets[b].count++;
            buckets[b].bbox.grow(p.bbox);
        });

        float totalArea = bboxArea(bbox);
        for (int i = 0; i < BUCKETNUM - 1; ++i) {
            range3f b0 = invalidrange3f, b1 = invalidrange3f;
            int count0 = 0, count1 = 0;
            for (int j = 0; j <= i; ++j) {
                b0.grow(buckets[j].bbox);
                count0 += buckets[j].count;
            }
            for (int j = i+1; j < BUCKETNUM; ++j) {
                b1.grow(buckets[j].bbox);
                count1 += buckets[j].count;
            }
            cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
        }

        float minCost = cost[0]; int minCostSplit = 0;
        for (int i = 1; i < BUCKETNUM - 1; ++i) {
            if (cost[i] < minCost) {
                minCost = cost[i];
                minCostSplit = i;
            }
        }

        if (minCost < (end - begin)) {
            auto sahFunc = [minCostSplit, BUCKETNUM, dim, cbbox] (const BuildItem &p) -> bool{
                float r = cbbox.max[dim] - cbbox.min[dim];
                int b = min<int>(BUCKETNUM-1, (BUCKETNUM * ((p.centroid[dim] - cbbox.min[dim]) / r)));
                error_if(b < 0 || b >= BUCKETNUM, "invalid bucket");
                return b <= minCostSplit;
            };
            mid = std::partition(begin, end, sahFunc);
            error_if(mid == begin || mid == end, "invalid sah split"); // generate tree;
        } else {
            mid = begin + (0.5f * (end - begin));
        }
    }
    warning_if_not(mid != begin && mid != end, "invalid split");
    auto left = sahTreeBuild(begin, mid, cdf, triangles, computeLOD, rng);
    auto right = sahTreeBuild(mid, end, cdf, triangles, computeLOD, rng);
    // Random select LOD
//    float ratio = left->area / (left->area + right->area);
//    auto &lod = rng.next1f() < ratio ? left->lod : right->lod;
    return new SBvhBranch { bbox, area, dim, left, right, lod };
}

SBvhNode* buildRegion(Subregion &region, TreeSerializer &ser, const std::map<Material*, int> &matIndex, Rng &rng) {
    // merge surfaces
    int vertNum = 0; int faceNum = 0;
    for(auto &surface : region.surfaces) {
        surface->load();
        vertNum += surface->pos.size();
        faceNum += surface->face.size();
    }
    vector<BTriangle> tris; tris.reserve(faceNum);
    vector<pair<StreamSurface*, uint>> verts; verts.reserve(vertNum);
    vector<BuildItem> items; items.reserve(faceNum);
    for(auto &surface : region.surfaces) {
        int s = verts.size();
        for(int i = 0; i < surface->pos.size(); i++) {
            verts.emplace_back(surface, i);
        }
        for(auto &f : surface->face) {
            const vec3f &v0 = surface->pos[f.x];
            const vec3f &v1 = surface->pos[f.y];
            const vec3f &v2 = surface->pos[f.z];
            range3f bbox = triangleBoundingBox(v0, v1, v2);
            float area = triangleArea(v0, v1, v2);
            items.emplace_back(bbox, bbox.center(), (int)tris.size(), area);
            int mindex = matIndex.at(surface->material);
            uint ftype = VPOS;
            if (!surface->norm.empty()) { ftype |= VNORM; }
            if (!surface->uv.empty()) { ftype |= VUV; }
            tris.emplace_back(vec3i{s+f.x, s+f.y, s+f.z}, mindex, (FTYPE)ftype);
        }
    }

    auto computeLeafLOD = [&tris, &verts](uint index, Rng &rng)->SLod {
        auto tri = tris.at(index);
        const vec3i &f = tri.f;
        uint16_t midx = tri.mindex;
        auto ftype = tri.ftype;

        StreamSurface *ss, *s1, *s2;
        int v0, v1, v2;

        std::tie(ss, v0) = verts.at(f.x);
        std::tie(s1, v1) = verts.at(f.y);
        std::tie(s2, v2) = verts.at(f.z);
        error_if(ss != s1 || ss != s2, "incorrect surface");

        Vertex v;
        vec2f st = rng.next2f();
        v.pos = lerp(ss->pos[v0],ss->pos[v1],ss->pos[v2],st);
        if (ftype & VNORM) v.norm = normalize(lerp(ss->norm[v0],ss->norm[v1],ss->norm[v2],st));
        else v.norm = triangleNormal(ss->pos[v0],ss->pos[v1],ss->pos[v2]);

        if (ftype & VUV) v.uv = lerp(ss->uv[v0],ss->uv[v1],ss->uv[v2],st);
        else v.uv = st;
        return { v, midx };
    };

    // build incore tree
    vector<float> cdf(items.size() + 1, 0.0f); //cdf buffer
    vector<uint> sorted; sorted.reserve(faceNum);
    SBvhNode *root = sahTreeBuild<decltype(computeLeafLOD)>(items.begin(), items.end(), cdf,
                                                            sorted, computeLeafLOD, rng);
#ifdef VERIFY_BVH
    SBvhNode *nroot = copyBVH(root);
    off_t off = ser.fileOff;
#endif
    SBvhBrick* brick = ser.process(root, &sorted, &tris, &verts);
#ifdef VERIFY_BVH
    size_t len = ser.fileOff - off;
//    message_va("brick len %d", len);
    char *buffer = new char[len];
    error_if(buffer == 0, "failed to allocate memory");
    int readed = pread(ser.fd, buffer, len, off);
    warning_if_not_va(readed == len, "incorrect read in %d/%d", readed/len);
    error_if_not(verify(brick->brick, (uint8_t)0, nroot, off, buffer, &sorted, &tris, &verts), "invalid bvh serialization");
    delete [] buffer;
    delete nroot;
#endif
    // serialize tree
    for(auto &surface : region.surfaces) { surface->unload(); }
    return brick;
}

std::vector<SBvhNode*> StreamAcceleratorBuilder::buildRegions(std::vector<Subregion> &regions, TreeSerializer &ser) {
    ps.progress_start("Build regions");
    std::vector<SBvhNode*> roots(regions.size());
    std::vector<Rng> rngs = initRngs(regions.size());

    constexpr bool parallel = true;
    if (parallel) {
        int s = 0;
        parallel_for(blocked_range<int>(0, regions.size()), [&](blocked_range<int> &range) {
            for (int r = range.begin(); r != range.end(); r++) {
                Subregion &region = regions[r];
                auto tree = buildRegion(region, ser, matIndex, rngs[r]);
                roots[r] = tree;
                ps.progress_update((float)(++s) / regions.size());
            }
        });
    } else {
        for(int r = 0; r < regions.size(); ++r) {
            Subregion &region = regions[r];
            auto tree = buildRegion(region, ser, matIndex, rngs[r]);
            roots[r] = tree;
            ps.progress_update((float)(r+1) / regions.size());
        }
    }
    ps.progress_end();
    return roots;
}


SBvhNode* sahTreeBuild(vector<BuildItem>::iterator begin, vector<BuildItem>::iterator end,
                       const std::vector<SBvhNode*> &roots, Rng &rng) {
    if (begin == end) { error("invalid size"); return 0; }
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    float area = 0;
    for_each(begin, end, [&bbox, &cbbox, &area](BuildItem &p) {
        bbox.grow(p.bbox);
        cbbox.grow(p.centroid);
        area += p.area;
    });

    int8_t dim = cbbox.extent().max_component_index();
    vector<BuildItem>::iterator mid;
    if (end - begin == 1) {
        return roots[begin->index];
    } else if (cbbox.max[dim] == cbbox.min[dim]) {
        mid = begin + (0.5f * (end - begin));
    } else {
        constexpr int BUCKETNUM = options::streamaccelerator::sahbucketnumber;
        SAHBucket buckets[BUCKETNUM];
        float cost[options::streamaccelerator::sahbucketnumber - 1];
        float range = cbbox.max[dim] - cbbox.min[dim];
        for_each(begin, end, [&bbox, &cbbox, range, dim, &buckets](BuildItem &p) {
            float offset = p.centroid[dim] - cbbox.min[dim];
            int b = min(BUCKETNUM - 1, (int)(BUCKETNUM * (offset / range)));
            error_if(b < 0 || b >= BUCKETNUM, "invalid bucket");
            buckets[b].count++;
            buckets[b].bbox.grow(p.bbox);
        });

        float totalArea = bboxArea(bbox);
        for (int i = 0; i < BUCKETNUM - 1; ++i) {
            range3f b0 = invalidrange3f, b1 = invalidrange3f;
            int count0 = 0, count1 = 0;
            for (int j = 0; j <= i; ++j) {
                b0.grow(buckets[j].bbox);
                count0 += buckets[j].count;
            }
            for (int j = i+1; j < BUCKETNUM; ++j) {
                b1.grow(buckets[j].bbox);
                count1 += buckets[j].count;
            }
            cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
        }

        float minCost = cost[0]; int minCostSplit = 0;
        for (int i = 1; i < BUCKETNUM - 1; ++i) {
            if (cost[i] < minCost) {
                minCost = cost[i];
                minCostSplit = i;
            }
        }

        if (minCost < (end - begin)) {
            auto sahFunc = [minCostSplit, BUCKETNUM, dim, cbbox] (const BuildItem &p) -> bool{
                float r = cbbox.max[dim] - cbbox.min[dim];
                int b = min<int>(BUCKETNUM-1, (BUCKETNUM * ((p.centroid[dim] - cbbox.min[dim]) / r)));
                error_if(b < 0 || b >= BUCKETNUM, "invalid bucket");
                return b <= minCostSplit;
            };
            mid = std::partition(begin, end, sahFunc);
            error_if(mid == begin || mid == end, "invalid sah split"); // generate tree;
        } else {
            mid = begin + (0.5f * (end - begin));
        }
    }
    warning_if_not(mid != begin && mid != end, "invalid split");
    auto left = sahTreeBuild(begin, mid, roots, rng);
    auto right = sahTreeBuild(mid, end, roots, rng);
    // Random Select LOD
    float ratio = left->area / (left->area + right->area);
    auto &lod = rng.next1f() < ratio ? left->lod : right->lod;
    return new SBvhBranch { bbox, area, dim, left, right, lod };
}


uint64_t StreamAcceleratorBuilder::buildRegionBvh(const std::vector<SBvhNode*> &roots, TreeSerializer &ser) {
    message_va("%d subtree", roots.size());
    vector<BuildItem> items; items.reserve(roots.size());
    for(int i = 0; i < roots.size(); i++) {
        auto r = roots[i];
        items.emplace_back(r->bbox, r->bbox.center(), i, r->area);
    }

    Rng rng;
    SBvhNode* root = sahTreeBuild(items.begin(), items.end(), roots, rng);
    SBvhBrick* brick = ser.process(root, 0, 0, 0);

    uint64_t brickId = brick->brick;
    delete brick;
    return brickId;
}

void StreamAcceleratorBuilder::loadRegion(const std::string &regionfilename,
                                          std::vector<Subregion> &regions) {
    jsonArray jarray = loadBin(regionfilename).as_array();
    std::unordered_map<std::string, Material*> matMap;
    for (Material* material : materials) matMap[material->name] = material;
    regions.resize(jarray.size());
    for (int r = 0; r < jarray.size(); r++) {
        const jsonObject &json = jarray[r].as_object_ref();
        Subregion &sr = regions[r];
        const jsonArray& sarray = jsonGet(json, "surfaces").as_array_ref();
        sr.bound.min = jsonGet(json, "bound_min").as_vec3f();
        sr.bound.max = jsonGet(json, "bound_max").as_vec3f();
        sr.count = jsonGet(json, "count");
        sr.surfaces.resize(sarray.size());
        for (int s = 0; s < sarray.size(); s++) {
            StreamSurface *ssurface = new StreamSurface();
            const jsonObject &js = sarray[s];
            ssurface->filename = jsonGet(js, "filename").as_string();
            ssurface->material = matMap[jsonGet(js, "material_ref").as_string()];
            sr.surfaces[s] = ssurface;
        }
    }
}

void StreamAcceleratorBuilder::saveRegion(const std::string &regionfilename,
                                          const std::vector<Subregion> &regions) {
    jsonArray jarray;
    for (const Subregion &reg : regions) {
        jsonArray sarr;
        for (StreamSurface *ss : reg.surfaces) {
            jsonObject js;
            js["filename"] = ss->filename;
            js["material_ref"] = ss->material->name;
            sarr.push_back(js);
        }
        jsonObject json;
        json["bound_min"] = reg.bound.min;
        json["bound_max"] = reg.bound.max;
        json["count"] = reg.count;
        json["surfaces"] = sarr;
        jarray.push_back(json);
    }
    saveBin(regionfilename, jarray);
}

void StreamAcceleratorBuilder::listMaterials(const std::vector<Surface*> &surfaces) {
    ps.start_single("List Materials");
    for (auto surface : surfaces) matIndex.insert( {surface->material, -1} );
    for (auto p : matIndex) materials.push_back(p.first);
    std::sort(materials.begin(), materials.end(),
              [](const Material* m1, const Material *m2) { return m1->name < m2->name; });
    warning_if(materials.size() >= pow(2, 13),
               "material index exceed limitatioin");
    for (int i = 0; i < materials.size(); i++) matIndex[materials[i]] = i;
    warning_if_va(materials.size() >= (1 << 13), "too many materials %d", materials.size());
    ps.end();
}


static unordered_map<unsigned int, Bucket>
bucketing(const Surface *surface, const vec3f &origin, float interval) {
    unordered_map<unsigned int, Bucket> map;
    if (surface->shape->type != TriangleMeshShape::TYPEID) return map;
    TriangleMeshShape *mesh = (TriangleMeshShape*)surface->shape;
    load(mesh);
    for(int i = 0; i < mesh->face.size(); i ++) {
        const vec3i &f = mesh->face[i];
        vec3f v0 = transformPoint(surface->transform, mesh->pos[f.x]);
        vec3f v1 = transformPoint(surface->transform, mesh->pos[f.y]);
        vec3f v2 = transformPoint(surface->transform, mesh->pos[f.z]);
        vec3f center = triangleCenter(v0, v1, v2);
        vec3i gidx = gridIndex(center, origin, interval);
        unsigned int code = morton3d::encode(gidx);
        auto ret = map.insert({ code, { gidx, code, range3f(center, center), 1 } });
        if (!ret.second) {
            Bucket &lb = ret.first->second;
            lb.count++;
            lb.bound.grow(center);
        }
    }
    unload(mesh);
    return map;
}

vector<Bucket> StreamAcceleratorBuilder::bucketing(vector<Surface*> &surfaces, const vec3f &origin,
                                                   float interval) const {
    ps.progress_start("Bucketing");
    vector<Bucket> buckets;
    buckets.reserve(surfaces.size());
    for (int s = 0; s < surfaces.size(); s++) {
        auto lbmap = ::bucketing(surfaces[s], origin, interval);
        for(auto &p : lbmap) buckets.push_back(p.second);
        ps.progress_update((float)(s+1) / surfaces.size());
    }
    ps.progress_end();
    return buckets;
}

bool StreamAcceleratorBuilder::buildStreamAccelerator(std::string &scenefilename, std::string &bvhfilename) {
    Scene *scene = loadScene(scenefilename, false);
    if (scene == 0) return false;
    sceneName = getFileBaseName(getFileName(scenefilename));

    std::vector<Surface*> surfaces;
    for(auto s : scene->surfaces) { //filter triangles
        if (s->shape->type == TriangleMeshShape::TYPEID) {
            surfaces.push_back(s);
        }
    }
    listMaterials(surfaces);
    string regionfilename = sceneName + "-region.jbin";

    ps.start("Stream BVH build");
    vector<Subregion> regions;
    if (fileNotExist(regionfilename.c_str())) {
        vector<range3f> sbounds(surfaces.size());
        range3f centoidBound; uint64_t triangleNum; uint64_t bytes;
        std::tie(centoidBound, triangleNum, bytes) = computerWorldBound(surfaces, sbounds, ps);
        vec3f ext = centoidBound.extent();
        float interval = ext[ext.max_component_index()] / options::streamaccelerator::gridresolution;
        vec3f origin = centoidBound.center() - makevec3(ext[ext.max_component_index()] * 0.5f);
        message_va("\tTriangle number: %d", triangleNum);
        message_va("\tMemory Usage: %.2lfGB", (double)bytes/space_consts<uint64_t>::giga);
        message_va("\tCentroid bound extent %s, \n\tcenter %s",
                   centoidBound.extent().print().c_str(),
                   centoidBound.center().print().c_str());
        vector<Bucket> buckets = bucketing(surfaces, origin, interval);
        chunking(surfaces, sbounds, buckets, regions, origin, interval);
        ps.fn_single("Save regions", [&regionfilename, &regions, this](){ saveRegion(regionfilename, regions); });
    } else {
        ps.fn_single("Load regions", [&regionfilename, &regions, this](){ loadRegion(regionfilename, regions); });
    }

    TreeSerializer ser;
    if(!ser.init(bvhfilename)) { error("failed to create tree serializer"); return false; }
    vector<SBvhNode*> roots = buildRegions(regions, ser);
    ps.fn_single("Build Top-level BVH", [&]() {
        uint64_t rootBrick = buildRegionBvh(roots, ser);
        message_va("root %lu", rootBrick);
        ser.writeRootBrick(rootBrick);
    });
    ps.fn_single("Cleanup", [&,this]() {
        warning_if_not_va(system(("rm -rf " + regionfilename).c_str()) == 0, "failed to remove %s", regionfilename.c_str());
        string folderPrefix = sceneName + "-flatten/";
        warning_if_not_va(system(("rm -rf " + folderPrefix).c_str()) == 0, "failed to remove %s", folderPrefix.c_str());
    });
    ps.end();
    return true;
}

