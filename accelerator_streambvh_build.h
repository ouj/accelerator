#ifndef _STREAM_ACCELERATOR_BUIDLER_H_
#define _STREAM_ACCELERATOR_BUIDLER_H_
#include <common/stdheader.h>
#include "accelerator_streambvh.h"
#include <scene/scene.h>
#include <scene/shape.h>
#include <common/progress.h>

namespace options {
    namespace streamaccelerator {
        constexpr int chuncksize = 2000000;
        constexpr int maxleaftriangles = 24;
        constexpr int sahbucketnumber = 32;
        constexpr int maxbrickprimitives = 256;
        constexpr int gridresolution = 1024;
        //constexpr uint iobuffersize = 2147483648; //2g
        constexpr uint iobuffersize = 1073741824; //1g
    }
};

struct StreamSurface {
    StreamSurface() = default;
    std::vector<vec3i>  face;
    std::vector<vec3f>  pos;
    std::vector<vec3f>  norm;
    std::vector<vec2f>  uv;
    Material*           material = 0;
    std::string         filename;
    bool                save() const;
    bool                load();
    void                unload();
};

struct Bucket;
struct Subregion;
struct SBvhNode;
struct TreeSerializer;
class StreamAcceleratorBuilder {
public:
    StreamAcceleratorBuilder() = default;
    ~StreamAcceleratorBuilder() = default;
    bool buildStreamAccelerator(std::string &scenefilename, std::string &bvhfilename);
private:
    void                        listMaterials(const std::vector<Surface*> &materials);

    std::vector<Bucket>         bucketing(std::vector<Surface*> &ssurfaces,const vec3f &origin, float interval) const;
    void                        chunking(std::vector<Surface*> &surfaces, std::vector<range3f> &sbounds,
                                         std::vector<Bucket> &buckets, std::vector<Subregion> &regions,
                                         vec3f &origin, float interval) const;
    std::vector<SBvhNode*>      buildRegions(std::vector<Subregion> &regions, TreeSerializer &ser);
    void                        loadRegion(const std::string &regionfilename, std::vector<Subregion> &region);
    void                        saveRegion(const std::string &regionfilename, const std::vector<Subregion> &regions);
    
    uint64_t                    buildRegionBvh(const std::vector<SBvhNode*> &roots, TreeSerializer &ser);
    std::string                 sceneName;
    std::map<Material*, int>    matIndex;
    std::vector<Material*>      materials;
    range3f                     worldBound;
    mutable Progress            ps;
    Rng                         rng;
};

#endif
