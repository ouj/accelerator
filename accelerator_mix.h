#ifndef ACCELERATOR_MIX_H
#define ACCELERATOR_MIX_H

#include "accelerator_list.h"
#include "accelerator_bvh.h"
#include "accelerator_lux.h"

#include <memory>

struct MixAcceleratorOpts : AcceleratorOpts {
    MixAcceleratorOpts();
    virtual jsonObject  print() const;
    virtual void        parse(const jsonObject &opts);
    std::shared_ptr<AcceleratorOpts> lightAcceleratorOpts;
    std::shared_ptr<AcceleratorOpts> objectAcceleratorOpts;
};

class MixAccelerator : public Accelerator {
public:
    static const int TYPEID = 30003;
    MixAccelerator();
    MixAccelerator(const MixAcceleratorOpts &opts);
    virtual ~MixAccelerator() { clearAccelerator(); }

    virtual bool        intersectFirst(const ray3f& ray, intersection3f& intersection) const;
    virtual bool        intersectAny(const ray3f& ray) const;
    virtual range3f     computeWorldBound() const;

    virtual void        parseAcceleratorOpts(const jsonObject &opts);
    virtual jsonObject  printAcceleratorOpts();
    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces,
                                         const std::vector<Light*>& lights);
    virtual void        clearAccelerator();
    virtual jsonObject  statistics() const;

    Accelerator*        lightAccelerator;
    Accelerator*        objectAccelerator;

    MixAcceleratorOpts  _opts;
};

#endif
