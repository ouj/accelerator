#include "accelerator_streambvh.h"
#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>
#include <stdio.h>
#include <queue>
#include <common/morton.h>
#include <scene/sceneio.h>
#include <map>
#include <unordered_map>
#include <set>
#include <tuple>
#include <tbb/tbb.h>
#include <scene/scenestat.h>
#include <common/lru.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <queue>
#include <common/stddir.h>
#include <common/distribution.h>
#include <common/progress.h>

using namespace streambvh;
using namespace std;
using namespace tbb;

struct SAHBucket {
    SAHBucket() : count(0), bbox(invalidrange3f){}
    int			count;
    range3f     bbox;
};


inline float bboxArea(const range3f &b) {
    vec3f d = b.extent();
    return 2.f * (d.x * d.y + d.x * d.z + d.y * d.z);
}

//range3f computeWorldBound(const std::vector<Surface*>& surfaces, int &count) {
//    range3f worldBound = invalidrange3f;
//    count = 0;
//    Shape *prevShape = 0;
//    for (int s = 0; s < surfaces.size(); s++) {
//        const auto* surface = surfaces[s];
//        Shape* shape = surface->shape;
//        if (prevShape != shape) {
//            if(prevShape) unload(prevShape);
//            load(shape);
//            prevShape = shape;
//        }
//        range3f bbox = AcceleratorCommon::computeWorldBound(surface);
//        worldBound.grow(bbox);
//        int c = 0;
//        if (shape->type == TriangleMeshShape::TYPEID) {
//            const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
//            c = mesh->face.size();
//            count += c;
//        }
//    }
//    if(prevShape) unload(prevShape);
//    return worldBound;
//}

void listingMaterials(const std::vector<Surface*>& surfaces,
                      map<string, int> &matmap, std::vector<Material*> &materials) {
    for (auto surface : surfaces) {
        string &mname = surface->material->name;
        if (matmap.find(mname) == matmap.end()) {
            matmap.insert({mname, materials.size()});
            materials.push_back(surface->material);
        }
    }
}

struct Bucket {
    vec3i                       gindex;
    unsigned int                code;
    range3f                     bound;
    unsigned int                count;
    
};

typedef unordered_map<unsigned int, Bucket> LbMap;
typedef typename LbMap::iterator            LbIter;

vec3i gridIndex(const vec3f &pt, const vec3f &origin, float interval) {
    vec3f i = floor_component((pt - origin) / interval);
    return vec3i((int)i.x, (int)i.y, (int)i.z);
}

LbMap bucketing(const StreamSurface &surface, const vec3f &origin, float interval) {
    LbMap map;
    const TriangleMeshShape* mesh = surface.shape;
    for(int i = 0; i < mesh->face.size(); i ++) {
        const vec3i &f = mesh->face[i];
        range3f bbox = triangleBoundingBox(mesh->pos[f.x], mesh->pos[f.y], mesh->pos[f.z]);
        vec3i gidx = gridIndex(bbox.center(), origin, interval);
        unsigned int code = morton3d::encode(gidx);
        auto ret = map.insert({ code, { gidx, code, bbox, 1 } });
        if (!ret.second) {
            Bucket &lb = ret.first->second;
            lb.count++;
            lb.bound.grow(bbox);
        }
    }
    return map;
}

vector<Bucket> bucketing(const std::vector<StreamSurface>& surfaces,
                         const vec3f &origin, float interval) {
    vector<Bucket> buckets;
    buckets.reserve(surfaces.size());
    for (int s = 0; s < surfaces.size(); s++) {
        LbMap lbMap = bucketing(surfaces[s], origin, interval);
        for_each(lbMap.begin(), lbMap.end(), [&buckets](LbMap::value_type& lb){
            buckets.push_back(lb.second);
        });
    }
    return buckets;
}

template<typename E>
void recursiveBucketMerging(Bucket* ptr, int size, int chunkSize, E &emit) {
    error_if(size == 0, "invalid size");
    if (size == 0) return;
    
    range3i gbbox = invalidrange3i;
    range3f bbox = invalidrange3f;
    int count = 0;
    for (int p = 0; p < size; p++) {
        gbbox.grow(ptr[p].gindex);
        bbox.grow(ptr[p].bound);
        count += ptr[p].count;
    }
    
    int dim = gbbox.extent().max_component_index();
    if (count <= chunkSize || gbbox.max[dim] == gbbox.min[dim]) {
        emit(ptr, size, bbox, count);
        return;
    }
    
    constexpr int bucketNum = 128;
    SAHBucket buckets[bucketNum];
    float cost[bucketNum-1];
    for (int p = 0; p < size; ++p) {
        float range = gbbox.max[dim] - gbbox.min[dim];
        float offset = ptr[p].gindex[dim] - gbbox.min[dim];
        int b = min(bucketNum - 1, (int)(bucketNum * (offset / range)));
        error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
        buckets[b].count++;
        buckets[b].bbox.grow(ptr[p].bound);
    }
    
    float totalArea = bboxArea(bbox);
    for (int i = 0; i < bucketNum - 1; ++i) {
        range3f b0 = invalidrange3f;
        range3f b1 = invalidrange3f;
        int count0 = 0;
        int count1 = 0;
        for (int j = 0; j <= i; ++j) {
            b0.grow(buckets[j].bbox);
            count0 += buckets[j].count;
        }
        for (int j = i+1; j < bucketNum; ++j) {
            b1.grow(buckets[j].bbox);
            count1 += buckets[j].count;
        }
        cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
    }
    
    float minCost = cost[0]; int minCostSplit = 0;
    for (int i = 1; i < bucketNum - 1; ++i) {
        if (cost[i] < minCost) {
            minCost = cost[i];
            minCostSplit = i;
        }
    }
    
    int mid = 0;
    if (size > 1 || minCost < size) {
        auto *pmid = std::partition(ptr, ptr + size, [minCostSplit, bucketNum, dim, gbbox]
                                    (const Bucket &p) -> bool{
                                        float r = gbbox.max[dim] - gbbox.min[dim];
                                        int b = min<int>(bucketNum-1, bucketNum*((p.gindex[dim]-gbbox.min[dim])/r));
                                        error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
                                        return b <= minCostSplit;
                                    });
        mid = (int)(pmid - ptr);
        if (mid == 0 || mid == size) {
            warning("invalid split"); // generate tree;
            mid = 0.5f * size;
        }
    } else {
        mid = 0.5f * size;
    }
    if (mid == 0 || mid == size) warning("invalid split");
    recursiveBucketMerging(ptr, mid, chunkSize, emit);
    recursiveBucketMerging(ptr+mid, size-mid, chunkSize, emit);
}

struct BucketGroup {
    BucketGroup(const range3f &b, int count)
    : bound(b), count(count) { }
    range3f                 bound;
    int                     count;
    int                     index;
    off_t                   offset;
};

template<typename E>
void chunking(const std::vector<AcceleratorSurface*>& surfaces,
              vector<Bucket> &bucketlist, int chunksize,
              const vec3f &origin, float interval, map<string, int> &matmap,
              E emitChunk) {
    // merging bucket to chunk
    vector<BucketGroup>                         bucketGroups;
    unordered_map<uint, uint>                   bcMap;    
    auto emit = [&bucketGroups, &bcMap]
    (Bucket* ptr, int size, const range3f &bbox, int count) {
        range3f bound = invalidrange3f;
        unsigned int gid = bucketGroups.size();
        for (int s = 0; s < size; s++) {
            const Bucket &b = ptr[s];
            bound.grow(b.bound);
            bcMap[b.code] = gid;
        }
        bucketGroups.emplace_back(bound, count);
    };
    recursiveBucketMerging(bucketlist.data(), bucketlist.size(), chunksize, emit);
    
    int fd = open("CHUNK.BIN", O_WRONLY|O_CREAT, 0666);
    error_if(fd == -1, "cannot create file");
    //fcntl(fd, F_NOCACHE, 1);
    if(fd == -1) return;
    
    // reserve disk space and reset counter.
    off_t offset = 0;
    int count = 0;
    for_each(bucketGroups.begin(), bucketGroups.end(), [&offset, &count](BucketGroup &c) {
        c.offset = offset;
        offset += c.count * sizeof(triangle);
        count += c.count;
        c.index = 0;
    });
    message_va("bucket groups %d, triangle numbers %d", bucketGroups.size(), count);
    
    unordered_map<unsigned int, vector<triangle>> triangelCache;
    int cacheSize = 0;
    auto flush = [&triangelCache, &cacheSize, &bucketGroups, &fd, &emitChunk]() { //share flush routine
        auto it = triangelCache.begin();
        for (; it != triangelCache.end(); it++) {
            uint id = it->first;
            vector<triangle>& triangles = it->second;
            BucketGroup &bg = bucketGroups[id];
            off_t off = bg.offset + bg.index * sizeof(triangle);
            size_t length = triangles.size() * sizeof(triangle);
            pwrite(fd, &triangles[0], length, off);
            bg.index += triangles.size();
            if (bg.index == bg.count) { // chunk done
                emitChunk(bg.offset, bg.count, bg.bound);
            }
        }
        triangelCache.clear();
        cacheSize = 0;
    };
    
    Shape* prevShape = 0;
    for_each(surfaces.begin(), surfaces.end(), [&](const AcceleratorSurface *surface) {
        Shape* shape = surface->shape;
        if (prevShape != shape) {
            if(prevShape) unload(prevShape);
            load(shape);
            prevShape = shape;
        }
        const TriangleMeshShape* mesh = (const TriangleMeshShape*)shape;
        unsigned int nfaces = mesh->face.size();
        for (unsigned int i = 0; i < nfaces; i++) {
            triangle tri;
            const vec3i &f = mesh->face[i];
            const vec3f &v0 = mesh->pos[f[0]];
            const vec3f &v1 = mesh->pos[f[1]];
            const vec3f &v2 = mesh->pos[f[2]];
            // get world bounding box
            const vec3f &wv0 = tri.pos[0] = transformPoint(surface->transform, v0);
            const vec3f &wv1 = tri.pos[1] = transformPoint(surface->transform, v1);
            const vec3f &wv2 = tri.pos[2] = transformPoint(surface->transform, v2);
            range3f bbox = triangleBoundingBox(wv0, wv1, wv2);
            vec3f centroid = bbox.center();
            if (!mesh->norm.empty()) {
                tri.norm[0] = transformNormal(surface->transform, mesh->norm[f[0]]);
                tri.norm[1] = transformNormal(surface->transform, mesh->norm[f[1]]);
                tri.norm[2] = transformNormal(surface->transform, mesh->norm[f[2]]);
            } else tri.norm[0] = tri.norm[1] = tri.norm[2] = triangleNormal(wv0, wv1, wv2);
            if (!mesh->uv.empty()) {
                tri.uv[0] = mesh->uv[f[0]];
                tri.uv[1] = mesh->uv[f[1]];
                tri.uv[2] = mesh->uv[f[2]];
            } else tri.uv[0] = tri.uv[1] = tri.uv[2] = half2f;
            tri.mindex = matmap[surface->material->name];
            // find the chunk it belong to.
            vec3i gidx = gridIndex(centroid, origin, interval);
            unsigned int code = morton3d::encode(gidx.x, gidx.y, gidx.z);
            unsigned int cId = bcMap[code];
            triangelCache[cId].push_back(tri);
            cacheSize++;
            if (cacheSize * sizeof(triangle) > space_consts<int>::giga) {
                flush(); // used more than 1 giga byte memory. flush
            }
        }
    });
    flush();
    if(prevShape) unload(prevShape);
    close(fd);
}

struct ChunkBuildItem {
    off_t       offset;
    int         count;
    range3f     bbox;
    vec3f       centroid;
    triangle    repTri;
    uint32_t    brickId;
    float       opacity;
};

struct triangleItem {
    range3f bbox;
    vec3f   centroid;
    int     index;
    float   area;
};

struct buildNode {
    int8_t      axis;
    range3f     bbox;
    float       area;
    uint32_t    index;
    uint32_t    left;
    uint32_t    right;
};

int makeLeaf(triangleItem *ptr, int size, const range3f &bbox, vector<buildNode> &nodes,
             vector<int> &sortedIndices, float rs) {
    uint32_t nid = nodes.size();
    uint32_t begin = sortedIndices.size();
    vector<float> areas;
    float area = 0.0f;
    for (int p = 0; p < size; p++) {
        sortedIndices.push_back(ptr[p].index);
        areas.push_back(ptr[p].area);
        area += ptr[p].area;
    }
    Distribution1D<float> dist(areas);
    uint32_t off = min<uint32_t>(dist.sample(rs, 0), areas.size() - 1);
    uint32_t end = sortedIndices.size();
    buildNode leaf {LEAFNODE, bbox, area, begin + off, begin, end};
    nodes.push_back(leaf);
    return nid;
}

uint32_t recursiveTreeBuild(triangleItem *ptr, int size, int maxLeafItems, vector<buildNode> &nodes, vector<int> &sortedIndices, Rng &rng) {
    error_if(size == 0, "invalid size");
    //totalNodes++;
    
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    for (int p = 0; p < size; p++) {
        bbox.grow(ptr[p].bbox);
        cbbox.grow(ptr[p].centroid);
    }
    
    int8_t dim = cbbox.extent().max_component_index();
    int mid = 0;
    if (size <= maxLeafItems)
        return makeLeaf(ptr, size, bbox, nodes, sortedIndices, rng.next1f());
    else if (cbbox.max[dim] == cbbox.min[dim]) {
        mid = 0.5f * size;
    } else {
        const int bucketNum = 32;
        SAHBucket buckets[bucketNum];
        float cost[bucketNum-1];
        for (int p = 0; p < size; ++p) {
            float range = cbbox.max[dim] - cbbox.min[dim];
            float offset = ptr[p].centroid[dim] - cbbox.min[dim];
            int b = min(bucketNum - 1, (int)(bucketNum * (offset / range)));
            error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
            buckets[b].count++;
            buckets[b].bbox.grow(ptr[p].bbox);
        }
        
        float totalArea = bboxArea(bbox);
        for (int i = 0; i < bucketNum - 1; ++i) {
            range3f b0 = invalidrange3f;
            range3f b1 = invalidrange3f;
            int count0 = 0;
            int count1 = 0;
            for (int j = 0; j <= i; ++j) {
                b0.grow(buckets[j].bbox);
                count0 += buckets[j].count;
            }
            for (int j = i+1; j < bucketNum; ++j) {
                b1.grow(buckets[j].bbox);
                count1 += buckets[j].count;
            }
            cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
        }
        
        float minCost = cost[0]; int minCostSplit = 0;
        for (int i = 1; i < bucketNum - 1; ++i) {
            if (cost[i] < minCost) {
                minCost = cost[i];
                minCostSplit = i;
            }
        }
        
        if (size > maxLeafItems || minCost < size) {
            auto *pmid = std::partition(ptr, ptr + size, [minCostSplit, bucketNum, dim, cbbox]
                                        (const triangleItem &p) -> bool{
                                            float r = cbbox.max[dim] - cbbox.min[dim];
                                            int b = static_cast<int>(bucketNum *
                                                                     ((p.centroid[dim] - cbbox.min[dim]) / r));
                                            if (b == bucketNum) b = bucketNum-1;
                                            error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
                                            return b <= minCostSplit;
                                        });
            mid = (int)(pmid - ptr);
            if (mid == 0 || mid == size) {
                warning("invalid split"); // generate tree;
                return makeLeaf(ptr, size, bbox, nodes, sortedIndices, rng.next1f());
            }
        } else {
            mid = 0.5f * size;
        }
    }
    warning_if_not(mid != 0 && mid != size, "invalid split");
    uint32_t left = recursiveTreeBuild(ptr, mid, maxLeafItems, nodes, sortedIndices, rng);
    uint32_t right = recursiveTreeBuild(ptr + mid, size - mid, maxLeafItems, nodes, sortedIndices, rng);
    buildNode& ln = nodes[left];
    buildNode& rn = nodes[right];
    float sa = ln.area + rn.area;
    uint32_t tidx = (rng.next1f() < ln.area / sa) ? ln.index : rn.index;
    buildNode branch {dim, bbox, sa, tidx, left, right};
    nodes.push_back(branch);
    return  nodes.size() - 1;
}


inline int getNodeSize(const buildNode &node) {
    if (node.axis == LEAFNODE)
        return sizeof(CompactNode) + sizeof(triangle) * (node.right - node.left);
    else return sizeof(CompactNode);
}

template<typename L>
void depthFirstTravel(FBrick *brick, int currentBid, uint16_t cnIdx, vector<buildNode> &nodes, int nIdx,
                      vector<triangle> &rtris, vector<int> &sortedIndices, L &loadBrick) {
    buildNode &node = nodes[nIdx];
    const triangle* triangles = 0;
    const CompactNode* cnodes = 0;
    cnodes = getNodePointer(brick);
    triangles = getTrianglePointer(brick);
    
    const CompactNode *cnode = &cnodes[cnIdx];
    
    int prevBid = currentBid;
    if (cnode->axis == BRICKNODE) {
        const triangle &t1 = rtris[sortedIndices[node.index]];
        const triangle &t2 = triangles[cnode->triRep];
        warning_if_va(t1.pos[0] != t2.pos[0], "unmatch vertex pos %s != %s",
                      t1.pos[0].print().c_str(), t2.pos[0].print().c_str());
        
        currentBid = cnode->brickId;
        brick = loadBrick(cnode->brickId);
        cnodes = getNodePointer(brick);
        triangles = getTrianglePointer(brick);
        cnode = &cnodes[0];
        //node transfer
    }

    error_if_not_va(cnode->axis == node.axis, "incorrect type %d != %d", cnode->axis, node.axis);
    error_if_not(cnode->bbox.min == node.bbox.min, "incorrect bbox");
    error_if_not(cnode->bbox.max == node.bbox.max, "incorrect bbox");
    if (cnode->axis == LEAFNODE) {
        warning_if_not_va(cnode->count == node.right - node.left, "incorect leaf count: %d != %d",
                          cnode->count, node.right - node.left);
        for (int i = 0; i < cnode->count; i++) {
            int i1 = node.left + i;
            const triangle &t1 = rtris[sortedIndices[i1]];
            int i2 = cnode->offset + i;
            const triangle &t2 = triangles[i2];
            warning_if_va(t1.pos[0] != t2.pos[0], "unmatch vertex pos %s != %s",
                       t1.pos[0].print().c_str(), t2.pos[0].print().c_str());
        }
    } else {
        //message_va("axis %d, childbrick: %d, left: %d, right: %d", cnode.axis, cnode.brickId, cnode.left, cnode.right);
        uint16_t left = cnode->left;
        uint16_t right = cnode->right;
        depthFirstTravel(brick, currentBid, left, nodes, node.left, rtris, sortedIndices, loadBrick);
        depthFirstTravel(brick, currentBid, right, nodes, node.right, rtris, sortedIndices, loadBrick);
    }

    if (prevBid != currentBid) {
        brick = loadBrick(prevBid);
        cnodes = getNodePointer(brick);
        triangles = getTrianglePointer(brick);
    }
}

void verify(int fd, uint32_t brickId, vector<buildNode> &nodes, int idx,
            vector<triangle> &rtris, vector<int> &sortedIndices) {
    char stackbuffer[BRICKSIZE];
    char *buffer = &stackbuffer[0];
    int currentBrickId = -1;
    CompactNode* cnodes;
    triangle* triangles;
    auto loadBrick = [fd, buffer, &cnodes, &triangles, &currentBrickId](uint32_t brickId)->FBrick*{
        if (currentBrickId != brickId) {
            off_t offset = (off_t)brickId * BRICKSIZE;
            size_t len = BRICKSIZE;
            int readed = pread(fd, buffer, len, offset);
            error_if_not(readed <= len, "incorrect read in");
            currentBrickId = brickId;
        }
        FBrick *fb = (FBrick*)(buffer);
        return fb;
    };

    FBrick* lcnodes = loadBrick(brickId);
    depthFirstTravel(lcnodes, brickId, 0, nodes, idx, rtris, sortedIndices, loadBrick);
}


struct Brickenizer {
    Brickenizer(int fd) : fd(fd) {
        brickIndex = 0;
    }
    uint32_t brickenize(uint roodIdx, vector<buildNode> &nodes,
                        const vector<triangle> &triangles, const vector<int> &sortedIndices);
    template<typename T>
    size_t              subtree(const vector<buildNode> &nodes, uint idx, T &emitBrick);
    int                 fd;
    tbb::atomic<uint>        brickIndex;
};

template<typename T>
size_t Brickenizer::subtree(const vector<buildNode> &nodes, uint idx,
                            T &emitBrick) {
    const buildNode* node = &nodes[idx];
    if (node->axis == LEAFNODE) { // leaf
        size_t count = node->right - node->left;
        return sizeof(CompactNode) + sizeof(triangle) * count;
    } else {
        size_t lsize = subtree(nodes, node->left, emitBrick);
        size_t rsize = subtree(nodes, node->right, emitBrick);
        size_t total = lsize + rsize + sizeof(CompactNode);
        if (total + sizeof(FBrick) <= BRICKSIZE) {
            return total;
        } else {
            emitBrick(node->left);
            emitBrick(node->right);
            return sizeof(CompactNode) * 3 + sizeof(triangle) * 2;
        }
    }
};


uint32_t Brickenizer::brickenize(uint rootIdx, vector<buildNode> &nodes,
                                 const vector<streambvh::triangle> &triangles,
                                 const vector<int> &sortedIndices) {
    auto emitBrick = [this, &nodes, &triangles, &sortedIndices](int ridx) {
        vector<CompactNode> cnodes;
        vector<triangle>    btriangles;
        queue<int> q;
        q.push(ridx);
        
        
        while (!q.empty()) {
            auto c = q.front();
            q.pop();
            const buildNode *node = &nodes[c];
            float opacity = min(1.0f, node->area / bboxArea(node->bbox));
            
            if (node->axis == LEAFNODE) {
                uint16_t offset = btriangles.size();
                uint16_t count = node->right - node->left;
                for(int i = node->left; i < node->right; i++) {
                    btriangles.push_back(triangles[sortedIndices[i]]);
                }
                CompactNode cNode;
                cNode.axis = LEAFNODE;
                cNode.bbox = node->bbox;
                cNode.opacity = opacity;
                cNode.brickLoaded = false;
                cNode.offset = offset;
                cNode.count = count;
                cnodes.push_back(cNode);
            } else if (node->axis == BRICKNODE) {
                CompactNode cNode;
                cNode.axis = BRICKNODE;
                cNode.bbox = node->bbox;
                cNode.opacity = opacity;
                cNode.brickLoaded = false;
                error_if(node->right != node->left, "incorrect brick Id");
                cNode.brickId = node->left;
                cNode.triRep = btriangles.size();
                btriangles.push_back(triangles[sortedIndices[node->index]]);
                cnodes.push_back(cNode);
            } else {
                CompactNode cNode;
                q.push(node->left);
                cNode.left = cnodes.size() + q.size();
                q.push(node->right);
                cNode.right = cnodes.size() + q.size();
                cNode.axis = node->axis;
                cNode.bbox = node->bbox;
                cNode.opacity = opacity;
                cNode.brickLoaded = false;
                cnodes.push_back(cNode);
            }
        }
        uint32_t bIndex = brickIndex.fetch_and_increment();
        off_t off = (off_t)bIndex * BRICKSIZE;
        
        uint32_t length = sizeof(FBrick) + sizeof(CompactNode) * cnodes.size() + sizeof(triangle) * btriangles.size();
        FBrick brick { length, (uint16_t)cnodes.size(), (uint16_t)btriangles.size() };
        
        char buffer[BRICKSIZE];
        char* ptr = buffer;
        memcpy(ptr, &brick, sizeof(FBrick)); ptr += sizeof(FBrick);
        memcpy(ptr, &cnodes[0], cnodes.size() * sizeof(CompactNode)); ptr += cnodes.size() * sizeof(CompactNode);
        memcpy(ptr, &btriangles[0], btriangles.size() * sizeof(triangle)); ptr += btriangles.size() * sizeof(triangle);
        error_if(length != ptr - buffer, "incorect length");
        
        int written = pwrite(fd, buffer, length, off);
        warning_if_va(written != length, "incorrect written bytes");
        
        // set brick id
        buildNode &rootNode = nodes[ridx];
        rootNode.axis = BRICKNODE;
        rootNode.left = rootNode.right = bIndex;
//        message_va("output brick of %d nodes %d triangles, brick id %d, size %f%%",
//                   cnodes.size(), btriangles.size(), bIndex, 100.0 * length / BRICKSIZE);
    };
    
    subtree(nodes, rootIdx, emitBrick);
    emitBrick(rootIdx);
    buildNode &rootNode = nodes[rootIdx];
    error_if(rootNode.right != rootNode.left, "incorrect brick Id");
    return rootNode.left;
};

void buildTree(int fd, ChunkBuildItem &chunk, int maxLeafItems,
               Brickenizer &brickenizer, Rng &rng) {
    warning_if(chunk.count <= 0, "no triangle in the chunk");
    // load triangles;
    vector<triangle> triangles(chunk.count);
    vector<triangleItem> items(chunk.count);
    pread(fd, &triangles[0], sizeof(triangle)*chunk.count, chunk.offset);
    for (int i = 0; i < chunk.count; i++) {
        const triangle &t = triangles[i];
        range3f bbox = triangleBoundingBox(t.pos[0], t.pos[1], t.pos[2]);
        float area = triangleArea(t.pos[0], t.pos[1], t.pos[2]);
        items[i] = {bbox, bbox.center(), i, area};
    }
    vector<buildNode> nodes;
    vector<int> sortedIndices;
    recursiveTreeBuild(&items[0], items.size(), maxLeafItems, nodes, sortedIndices, rng);
    message_va("%d triangles, tree nodes %d", items.size(), nodes.size());
    buildNode &root = nodes[nodes.size() - 1];
    chunk.opacity = min(1.0f, root.area / bboxArea(root.bbox));
    vector<buildNode> vNodes = nodes;
    int brickId = brickenizer.brickenize(nodes.size() - 1, nodes, triangles, sortedIndices);
    verify(brickenizer.fd, brickId, vNodes, vNodes.size() - 1, triangles, sortedIndices);
    chunk.brickId = brickId;
    chunk.repTri = triangles[sortedIndices[root.index]];
}

struct chunkBuildNode {
    int8_t      axis;
    range3f     bbox;
    uint32_t    left;
    uint32_t    right;
};

uint32_t recursiveTreeBuild(ChunkBuildItem *ptr, int size, vector<chunkBuildNode> &nodes, vector<Chunk> &chunks) {
    error_if(size == 0, "invalid size");
    //totalNodes++;
    
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    for (int p = 0; p < size; p++) {
        bbox.grow(ptr[p].bbox);
        cbbox.grow(ptr[p].centroid);
    }
    
    int8_t dim = cbbox.extent().max_component_index();
    int mid = 0;
    if (size == 1) {
        uint32_t nid = nodes.size();
        Chunk chunk { ptr[0].bbox, ptr[0].repTri, ptr[0].brickId, ptr[0].opacity };
        chunkBuildNode leaf {LEAFNODE, bbox, (uint32_t)chunks.size(), (uint32_t)chunks.size()};
        chunks.push_back(chunk);
        nodes.push_back(leaf);
        return nid;
    } else if (cbbox.max[dim] == cbbox.min[dim]) {
        mid = 0.5f * size;
    } else {
        const int bucketNum = 32;
        SAHBucket buckets[bucketNum];
        float cost[bucketNum-1];
        for (int p = 0; p < size; ++p) {
            float range = cbbox.max[dim] - cbbox.min[dim];
            float offset = ptr[p].centroid[dim] - cbbox.min[dim];
            int b = min(bucketNum - 1, (int)(bucketNum * (offset / range)));
            error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
            buckets[b].count++;
            buckets[b].bbox.grow(ptr[p].bbox);
        }
        
        float totalArea = bboxArea(bbox);
        for (int i = 0; i < bucketNum - 1; ++i) {
            range3f b0 = invalidrange3f;
            range3f b1 = invalidrange3f;
            int count0 = 0;
            int count1 = 0;
            for (int j = 0; j <= i; ++j) {
                b0.grow(buckets[j].bbox);
                count0 += buckets[j].count;
            }
            for (int j = i+1; j < bucketNum; ++j) {
                b1.grow(buckets[j].bbox);
                count1 += buckets[j].count;
            }
            cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
        }
        
        float minCost = cost[0]; int minCostSplit = 0;
        for (int i = 1; i < bucketNum - 1; ++i) {
            if (cost[i] < minCost) {
                minCost = cost[i];
                minCostSplit = i;
            }
        }
        
        if (size > 1 || minCost < size) {
            auto *pmid = std::partition(ptr, ptr + size, [minCostSplit, bucketNum, dim, cbbox]
                                        (const ChunkBuildItem &p) -> bool{
                                            float r = cbbox.max[dim] - cbbox.min[dim];
                                            int b = static_cast<int>(bucketNum *
                                                                     ((p.centroid[dim] - cbbox.min[dim]) / r));
                                            if (b == bucketNum) b = bucketNum-1;
                                            error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
                                            return b <= minCostSplit;
                                        });
            mid = (int)(pmid - ptr);
            if (mid == 0 || mid == size) {
                warning("invalid split"); // generate tree;
                mid = 0.5f * size;
            }
        } else {
            mid = 0.5f * size;
        }
    }
    warning_if_not(mid != 0 && mid != size, "invalid split");
    uint32_t left = recursiveTreeBuild(ptr, mid, nodes, chunks);
    uint32_t right = recursiveTreeBuild(ptr + mid, size - mid, nodes, chunks);
    chunkBuildNode branch {dim, bbox, left, right};
    nodes.push_back(branch);
    return nodes.size() - 1;
};

int flatten(vector<chunkBuildNode> &buildNodes, int index, std::vector<StreamBvhNode> &nodes, int &offset) {
    chunkBuildNode &buildNode = buildNodes[index];
    StreamBvhNode *lnode = &nodes[offset];
    lnode->bbox = buildNode.bbox;
    lnode->axis = buildNode.axis;
    uint32_t off = offset++;
    if (buildNode.axis == LEAFNODE) {
        warning_if(buildNode.left != buildNode.right, "incorrect brick ID");
        lnode->chunkId = buildNode.left;
    } else {
        flatten(buildNodes, buildNode.left, nodes, offset);
        lnode->secondChild = flatten(buildNodes, buildNode.right, nodes, offset);
    }
    return off;
}

std::pair<TriangleMeshShape*, range3f>
flattenShape(TriangleMeshShape* shape, const Transform *transform, const string &surfaceName) {
    TriangleMeshShape *nshape = new TriangleMeshShape();
    range3f bound = invalidrange3f;
    nshape->uv = shape->uv;
    nshape->face = shape->face;
    nshape->pos.resize(shape->pos.size());
    nshape->norm.resize(shape->norm.size());
    
    for (int i = 0; i < shape->pos.size(); i++) {
        nshape->pos[i] = transformPoint(transform, shape->pos[i]);
        bound.grow(nshape->pos[i]);
        if (!shape->norm.empty()) 
            nshape->norm[i] = transformNormal(transform, shape->norm[i]);
    }
    return { nshape, bound };
}

vector<StreamSurface> StreamBvhAccelerator::flattenSurfaces(const std::vector<Surface*>& surfaces,
                                                            range3f &worldBound, Progress &progress) {
    vector<StreamSurface> ssurfaces;
    worldBound = invalidrange3f;
    for (int s = 0; s < surfaces.size(); s++) {
        Surface *surface = surfaces[s];
        if (surface->shape->type == TriangleMeshShape::TYPEID) {
            load(surface->shape);
            auto ret = flattenShape((TriangleMeshShape*)surface->shape,
                                    surface->transform, surface->name);
            unload(surface->shape);
            ssurfaces.emplace_back(ret.first, surface->material);
            worldBound.grow(ret.second);
        }
        progress.progress_update((float)s / surfaces.size());
    }
    return ssurfaces;
}

void StreamBvhAccelerator::buildStreamBvhAccelerator(const std::vector<Surface*>& surfaces,
                                                     range3f &worldBound) {
    Progress progress;
    
    message_va("Surfaces number: %d", surfaces.size());
    vector<ChunkBuildItem> chunkBuildItems;
    map<string, int> matmap;
    listingMaterials(surfaces, matmap, materials);
    
    progress.progress_start("Flatten surfaces");
    std::vector<StreamSurface> ssurfaces = flattenSurfaces(surfaces, worldBound, progress);
    progress.progress_end();
    
    message_va("\tTriangle number %d", triangleNum);
    message_va("\tWorld bound extent %s, center %s",
               worldBound.extent().print().c_str(),
               worldBound.center().print().c_str());

    vec3f ext = worldBound.extent();
    float interval = ext[ext.max_component_index()] / options::streambvh::gridresolution;
    vec3f origin = worldBound.center() - makevec3(ext[ext.max_component_index()] * 0.5f);
    
    progress.start_single("Bucketing");
    vector<Bucket> buckets = bucketing(ssurfaces, origin, interval);
    progress.end();
    message_va("\tBucket number %d", buckets.size());
    
    progress.fn_single("Group Buckets", [&, this] {
        vector<BucketGroup>                         bucketGroups;
        unordered_map<uint, uint>                   bcMap;
        auto emit = [&](Bucket* ptr, int size, const range3f &bbox, int count) {
            range3f bound = invalidrange3f;
            unsigned int gid = bucketGroups.size();
            for (int s = 0; s < size; s++) {
                const Bucket &b = ptr[s];
                bound.grow(b.bound);
                bcMap[b.code] = gid;
            }
            bucketGroups.emplace_back(bound, count);
        };
        recursiveBucketMerging(buckets.data(), buckets.size(), opts.chunkSize, emit);
    });
    
    /*
    if(fileNotExist("INCORE.BIN")){
        if(fileNotExist("CHUNKMETA.BIN")) {
            message("compute world bound");
            start = tbb::tick_count::now();
            worldBound = ::computeWorldBound(surfaces, triangleNum);
            stop = tbb::tick_count::now();
            time_elasped = stop - start;
            message_va("computing world bound time : %g", time_elasped.seconds());
            message_va("triangle number %d", triangleNum);
            
            // coarse grid
            vec3f ext = worldBound.extent();
            float interval = ext[ext.max_component_index()] / gridresolution;
            vec3f origin = worldBound.center() - makevec3(ext[ext.max_component_index()] * 0.5f);
            message_va("world bound extent %s, center %s, origin %s",
                       worldBound.extent().print().c_str(),
                       worldBound.center().print().c_str(),
                       origin.print().c_str());
            
            // build buckets
            message("bucketing");
            start = tbb::tick_count::now();
            vector<Bucket> buckets = bucketing(surfaces, origin, interval);
            stop = tbb::tick_count::now();
            time_elasped = stop - start;
            message_va("bucketing time : %g", time_elasped.seconds());
            message_va("bucket number %d", buckets.size());
            
            // build chunks
            message("chunking");
            start = tbb::tick_count::now();
            chunking(surfaces, buckets, opts.chunkSize, origin, interval, matmap,
                     [&chunkBuildItems](off_t off, int count, range3f &bound){
                         chunkBuildItems.push_back(ChunkBuildItem {off, count, bound, bound.center()}); });
            buckets.clear();
            // sort the chunks with file offset. does that matters?
            sort(chunkBuildItems.begin(), chunkBuildItems.end(), [](const ChunkBuildItem &c1, const ChunkBuildItem &c2) {
                return c1.offset < c2.offset;
            });
            stop = tbb::tick_count::now();
            time_elasped = stop - start;
            message_va("chunking time : %g", time_elasped.seconds());
            message_va("emit %d chunks", chunkBuildItems.size());
            
            int fd = open("CHUNKMETA.BIN", O_WRONLY|O_CREAT, 0666);
            error_if(fd == -1, "cannot create chunk file");
            uint size = chunkBuildItems.size();
            message_va("chunk build items: %d", chunkBuildItems.size());
            write(fd, &size, sizeof(uint));
            write(fd, &chunkBuildItems[0], chunkBuildItems.size() * sizeof(ChunkBuildItem));
            close(fd);
            
        } else {
            int fd = open("CHUNKMETA.BIN", O_RDONLY);
            error_if(fd == -1, "cannot create chunk file");
            uint size = 0;
            read(fd, &size, sizeof(uint));
            chunkBuildItems.resize(size);
            read(fd, &chunkBuildItems[0], chunkBuildItems.size() * sizeof(ChunkBuildItem));
            close(fd);
        }
        // build small trees
        message("trees building");
        start = tbb::tick_count::now();
        int fd = open("CHUNK.BIN", O_RDONLY);
        error_if(fd == -1, "cannot create chunk file");
        int nFd = open("TREES.BIN", O_RDWR|O_CREAT, 0666);
        error_if(nFd == -1, "cannot create tree file");
        
        Brickenizer brickenizer(nFd);
        const bool parallel = false;
        if (parallel) {
            parallel_for(blocked_range<int>(0, chunkBuildItems.size()),
                         [&](const blocked_range<int> &r) {
                             for (int i = r.begin(); i < r.end(); i++) {
                                 Rng rng(i);
                                 buildTree(fd, chunkBuildItems[i], opts.maxLeafItems, brickenizer, rng);
                                 fprintf(stdout, "\rChunk process: %d/%ld",
                                         i+1, chunkBuildItems.size());
                                 fflush(stdout);
                             }
                         });
        } else {
            for (int i = 0; i < chunkBuildItems.size(); i++) {
                Rng rng(i);
                buildTree(fd, chunkBuildItems[i], opts.maxLeafItems, brickenizer, rng);
                fprintf(stdout, "\rChunk process: %d/%ld, total %d node bricks              ",
                        i+1, chunkBuildItems.size(), (uint32_t)brickenizer.brickIndex);
                fflush(stdout);
            }
        }
        message_va("number of bricks: %ld", (uint32_t)brickenizer.brickIndex);
        close(nFd);
        close(fd);
        
        // remove unnessarry files.
        //        remove("CHUNK.BIN");
        //        remove("CHUNKMETA.BIN");
        
        stop = tbb::tick_count::now();
        time_elasped = stop - start;
        message_va("trees building time : %g", time_elasped.seconds());
        
        message("in-core tree building");
        start = tbb::tick_count::now();
        vector<chunkBuildNode> buildNodes;
        recursiveTreeBuild(&chunkBuildItems[0], chunkBuildItems.size(), buildNodes, chunks);
        nodes.resize(buildNodes.size());
        
        // flatten
        int offset = 0;
        flatten(buildNodes, buildNodes.size() - 1, nodes, offset);
        warning_if(buildNodes.size() != offset, "not match node number");
        stop = tbb::tick_count::now();
        time_elasped = stop - start;
        message_va("in-core tree building time : %g", time_elasped.seconds());
        int incoreFd = open("INCORE.BIN", O_WRONLY|O_CREAT, 0666);
        error_if(incoreFd == -1, "cannot create in-core tree file");
        uint size = nodes.size();
        write(incoreFd, &size, sizeof(uint));
        write(incoreFd, nodes.data(), nodes.size() * sizeof(StreamBvhNode));
        size = chunks.size();
        write(incoreFd, &size, sizeof(uint));
        write(incoreFd, chunks.data(), chunks.size() * sizeof(Chunk));
        close(incoreFd);
    } else {
        message("load in-core tree");
        int fd = open("INCORE.BIN", O_RDONLY);
        error_if(fd == -1, "cannot create in-core tree file");
        uint size = 0;
        read(fd, &size, sizeof(uint));
        nodes.resize(size);
        read(fd, nodes.data(), nodes.size() * sizeof(StreamBvhNode));
        read(fd, &size, sizeof(uint));
        chunks.resize(size);
        read(fd, chunks.data(), chunks.size() * sizeof(Chunk));
        close(fd);
        worldBound = nodes[0].bbox;
    }
    
    message_va("incore node numbers %d, memory %d", nodes.size(), nodes.size() * sizeof(StreamBvhNode));
    
    if(brickMgr.init("TREES.BIN", BRICKSIZE, 40960) == false)
        error("failed to init brick cache");
     */
}


