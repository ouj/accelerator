#ifndef ACCELERATOR_LUX_H
#define ACCELERATOR_LUX_H

#include "accelerator.h"
#include "accelerator_list.h"

struct LuxAcceleratorOpts : AcceleratorOpts {
    enum LuxType { BVH, QBVH };
    LuxAcceleratorOpts();
    jsonObject  print() const;
    void        parse(const jsonObject &opts);
    int         luxtype = QBVH;
};

namespace luxrays { class Accelerator; };
class LuxAccelerator : public Accelerator {
public:
    static const int TYPEID = 30004;
    LuxAccelerator();
    LuxAccelerator(const LuxAcceleratorOpts &opts);
    virtual ~LuxAccelerator() { clearAccelerator(); }

    virtual bool    intersectFirst(const ray3f& ray, sintersection3f& intersection, AcceleratorSurface*& surface) const;
    virtual bool    intersectFirst(const ray3f& ray, intersection3f& intersection) const;
    virtual bool    intersectAny(const ray3f& ray) const;
    virtual range3f computeWorldBound() const;

    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces, const std::vector<Light*>& lights);
    virtual void        clearAccelerator();
    virtual jsonObject  statistics() const;


protected:
    luxrays::Accelerator*               meshAccelerator;
    ListAccelerator*                    otherAccelerator;

    std::vector<AcceleratorSurface*>    meshSurfaces;

    int*                                meshIds;
    int*                                triangleIds;
    range3f                             meshBbox;

    LuxAcceleratorOpts                  _opts;
};


#endif
