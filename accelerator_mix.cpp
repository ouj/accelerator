#include "accelerator_mix.h"
#include "build.h"

MixAcceleratorOpts::MixAcceleratorOpts() : AcceleratorOpts(MixAccelerator::TYPEID) {
    lightAcceleratorOpts.reset(new ListAcceleratorOpts());
    objectAcceleratorOpts.reset(new BvhAcceleratorOpts());
}

jsonObject MixAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "mixacceleratoropts";
    json["light"] = lightAcceleratorOpts->print();
    json["object"] = objectAcceleratorOpts->print();
    return json;
}

void MixAcceleratorOpts::parse(const jsonObject &json) {
    jsonGet(json, "type").checkValue("mixacceleratoropts");
    if (jsonHas(json, "light")) {
        lightAcceleratorOpts.reset(parseAcceleratorOpts(jsonGet(json, "light")));
    }
    if (jsonHas(json, "objects")) {
        objectAcceleratorOpts.reset(parseAcceleratorOpts(jsonGet(json, "objects")));
    }
}

MixAccelerator::MixAccelerator() : Accelerator(TYPEID) {
    this->lightAccelerator = new ListAccelerator();
    this->objectAccelerator = new BvhAccelerator();
}

MixAccelerator::MixAccelerator(const MixAcceleratorOpts& opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
    this->lightAccelerator = createAccelerator(opts.lightAcceleratorOpts.get());
    this->objectAccelerator = createAccelerator(opts.objectAcceleratorOpts.get());
}

bool MixAccelerator::buildAccelerator(const std::vector<Surface*>& surfaces,
                                      const std::vector<Light*>& lights) {
    this->lightAccelerator->buildAccelerator(std::vector<Surface*>(), lights);
    this->objectAccelerator->buildAccelerator(surfaces, std::vector<Light*>());
    return true;
}

void MixAccelerator::clearAccelerator() {
    if(lightAccelerator) lightAccelerator->clearAccelerator();
    if(objectAccelerator) objectAccelerator->clearAccelerator();
    objectAccelerator = lightAccelerator = 0;
}

bool MixAccelerator::intersectFirst(const ray3f& ray,intersection3f& intersection) const {
    intersection3f lightIntersection, objectIntersection;
    bool lightHit = lightAccelerator->intersectFirst(ray, lightIntersection);
    bool objectHit = objectAccelerator->intersectFirst(ray, objectIntersection);
    if(!lightHit && !objectHit) return false;
    if(lightHit && (!objectHit || lightIntersection.t < objectIntersection.t)) {
        intersection = lightIntersection;
    } else {
        intersection = objectIntersection;
    }
    return true;
}

bool MixAccelerator::intersectAny(const ray3f& ray) const {
    bool hit = objectAccelerator->intersectAny(ray);
    if(!hit) return lightAccelerator->intersectAny(ray);
    return hit;
}

range3f MixAccelerator::computeWorldBound() const {
    return runion(lightAccelerator->computeWorldBound(),
                  objectAccelerator->computeWorldBound());
}

void MixAccelerator::parseAcceleratorOpts(const jsonObject &json) {
    _opts.parse(json);
}

jsonObject MixAccelerator::printAcceleratorOpts() {
    return _opts.print();
}

jsonObject MixAccelerator::statistics() const {
    jsonObject json;
    json["light"] = lightAccelerator->statistics();
    json["object"] = objectAccelerator->statistics();
    return json;
}

