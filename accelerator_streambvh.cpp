#include "accelerator_streambvh.h"
#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>
#include <scene/light.h>
#include <tuple>
#include <tbb/tbb.h>
#include <set>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

using namespace std;
using namespace tbb;
static constexpr int bvhstacksize = 64;

//using tbb::concurrent_lru_cache;
StreamBvhAcceleratorOpts::StreamBvhAcceleratorOpts()
    : AcceleratorOpts(StreamBvhAccelerator::TYPEID) {}

void StreamBvhAcceleratorOpts::parse(const jsonObject& json) {
    jsonGet(json, "type").checkValue("streambvhacceleratoropts");
    treefilename = jsonGet(json, "treefilename").as_string();
}

jsonObject StreamBvhAcceleratorOpts::print() const {
    jsonObject json;
    json["type"] = "streambvhacceleratoropts";
    json["treefilename"] = treefilename;
    return json;
}

StreamBvhAccelerator::StreamBvhAccelerator() : Accelerator(TYPEID) {}

StreamBvhAccelerator::StreamBvhAccelerator(const StreamBvhAcceleratorOpts& opts)
    : Accelerator(TYPEID), _opts(opts) {
    assert(opts.type == TYPEID);
}

StreamBvhAccelerator::~StreamBvhAccelerator() {
    clearAccelerator();
    if(listAccelerator) delete listAccelerator;
}

bool StreamBvhAccelerator::buildAccelerator(const std::vector<Surface*>& surfaces,
                                            const std::vector<Light*>& lights) {
    std::vector<Surface*> triangleSurfaces;
    std::vector<Surface*> otherSurfaces;
    for(int i = 0; i < surfaces.size(); i ++) {
        if (surfaces[i]->shape->type == TriangleMeshShape::TYPEID) {
            triangleSurfaces.push_back(surfaces[i]);
        } else {
            otherSurfaces.push_back(surfaces[i]);
        }
    }

    if (surfaces.empty()) {
        _worldBound = range3f(zero3f, zero3f);
    } else {
        listAccelerator = new ListAccelerator();
        listAccelerator->buildAccelerator(otherSurfaces, lights);
        listMaterials(triangleSurfaces);
        loadBvhFromFile();
    }
    return true;
}

bool StreamBvhAccelerator::loadBvhFromFile() {
    fd = open(_opts.treefilename.c_str(), O_RDONLY);
    if(fd < 0) { error("failed to open tree file"); return false; }
    fileLen = lseek(fd, 0, SEEK_END);
    message_va("tree file size %lu bytes, %lf GB", fileLen, (double)fileLen / 1024 / 1024 / 1024);
    brickBuffer = (char*)mmap(0, fileLen, PROT_READ, MAP_PRIVATE, fd, 0);
    if(brickBuffer == MAP_FAILED) { error("failed to creat memory mapping for nodes"); return false; }
    rootBrick = *((uint64_t*)brickBuffer);
    message_va("root id: %ld", rootBrick);
    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* verts = 0; const Lod *lods = 0;
    std::tie(snodes, striangles, verts, lods) = getArrays(brickBuffer + rootBrick);
    _worldBound = snodes[0].bound;
    return true;
}

void StreamBvhAccelerator::listMaterials(std::vector<Surface*> &surfaces) {
    std::set<Material*>    matSet;
    for (auto surface : surfaces) matSet.insert(surface->material);
    for (auto p : matSet) materials.push_back(p);
    std::sort(materials.begin(), materials.end(),
              [](const Material* m1, const Material *m2) { return m1->name < m2->name; });
}

static bool opacityResolve(const Texture* opacity, const vec2f &st) {
    if (!opacity) return true;
    if (opacity->image.size() == 0) return true;

    int i = static_cast<int>(st.x * opacity->image.width());
    int j = static_cast<int>(st.y * opacity->image.height());
    int w = opacity->image.width();
    int h = opacity->image.height();

    if (i < 0 || i >= w || j < 0 || j >= h) return false;
    return !opacity->image.at(i,h-j-1).iszero();
}


bool StreamBvhAccelerator::intersectFirst(const ray3f& rr, intersection3f& intersection) const {
    ray3f ray = rr;
    std::array<pair<uint64_t, uint32_t>, bvhstacksize> stack;
    bool hit = false;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    if(listAccelerator->intersectFirst(ray, intersection)) {
        hit = true;
        ray.maxt = intersection.t;
    }

    uint64_t cbrick = -1;
    uint64_t brick = rootBrick;
    int nodeNum = 0;

    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* verts = 0; const Lod *lods = 0;
    while (true) {
        if (cbrick != brick) {
            std::tie(snodes, striangles, verts, lods) = getArrays(brickBuffer + brick);
            cbrick = brick;
        }
        const SNode *snode = &snodes[nodeNum];
        if (intersectBoundingBox(snode->bound, ray, invDir)) {
            if (snode->type == LEAFNODE) {
                intersection3f isect;
                for (int s = 0; s < snode->count; s++) {
                    float t; vec2f uv;
                    const STriangle &tri = striangles[snode->offset + s];
                    const Vertex &v0 = verts[tri.face.x];
                    const Vertex &v1 = verts[tri.face.y];
                    const Vertex &v2 = verts[tri.face.z];
                    bool th = intersectTriangle(v0.pos, v1.pos, v2.pos, ray, t, uv);
                    if(th && (!hit || intersection.t > t)) {
                        const Material* material = materials[tri.mindex];
                        vec2f st = (tri.ftype & VUV) ? lerp(v0.uv, v1.uv, v2.uv, uv) : uv;
                        if (::opacityResolve(material->opacity, st)) {
                            hit = true;
                            intersection.t = t;
                            intersection.m = material;
                            intersection.f.o = ray.eval(t);
                            if(tri.ftype & VNORM) intersection.f.z = normalize(lerp(v0.norm, v1.norm, v2.norm, uv));
                            else intersection.f.z = triangleNormal(v0.pos,v1.pos,v2.pos);
                            xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
                            intersection.st = st;
                            ray.maxt = intersection.t;
                        }
                    }
                }
                if (sptr <= 0) break;
                tie(brick, nodeNum) = stack[--sptr];
            } else if (snode->type == BRICKNODE) {
                const Lod& lod = lods[snode->lod];
                brick = lod.brick;
                nodeNum = 0; // move to a new brick.
            } else {
                if (invDir[snode->axis] < 0) {
                    stack[sptr++] = { cbrick, snode->left };
                    nodeNum = snode->right;
                } else {
                    stack[sptr++] = { cbrick, snode->right };
                    nodeNum = snode->left;
                }
            }
        } else {
            if (sptr <= 0) break;
            tie(brick, nodeNum) = stack[--sptr];
        }
    }
    if (hit && dot(intersection.f.z, ray.d) > 0) intersection.f = flip(intersection.f);
    return hit;
}

bool StreamBvhAccelerator::intersectFirstLOD(const ray3f& rr, intersectionlod3f& intersection, float sigma2) {
    ray3f ray = rr;
    std::array<pair<uint64_t, uint32_t>, bvhstacksize> stack;
    bool hit = false;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    intersection3f isect;
    if(listAccelerator->intersectFirst(ray, isect)) {
        hit = true;
        intersection.t = isect.t;
        intersection.m = isect.m;
        intersection.f = isect.f;
        intersection.st = isect.st;
        intersection.mint = ray3f::epsilon;
        ray.maxt = intersection.t;
    }

    uint64_t cbrick = -1;
    uint64_t brick = rootBrick;
    int nodeNum = 0;

    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* verts = 0; const Lod *lods = 0;

    while (true) {
        if (cbrick != brick) {
            std::tie(snodes, striangles, verts, lods) = getArrays(brickBuffer + brick);
            cbrick = brick;
        }
        const SNode *snode = &snodes[nodeNum];
        if (intersectBoundingBox(snode->bound, ray, invDir)) {
            if (snode->axis == LEAFNODE) {
                intersection3f isect;
                for (int s = 0; s < snode->count; s++) {
                    float t; vec2f uv;
                    const STriangle &tri = striangles[snode->offset + s];
                    const Vertex &v0 = verts[tri.face.x];
                    const Vertex &v1 = verts[tri.face.y];
                    const Vertex &v2 = verts[tri.face.z];
                    bool th = intersectTriangle(v0.pos, v1.pos, v2.pos, ray, t, uv);
                    if(th && (!hit || intersection.t > t)) {
                        const Material* material = materials[tri.mindex];
                        vec2f st = (tri.ftype & VUV) ? lerp(v0.uv, v1.uv, v2.uv, uv) : uv;
                        if (::opacityResolve(material->opacity, st)) {
                            hit = true;
                            intersection.t = t;
                            intersection.m = material;
                            intersection.f.o = ray.eval(t);
                            if(tri.ftype & VNORM) intersection.f.z = normalize(lerp(v0.norm, v1.norm, v2.norm, uv));
                            else intersection.f.z = triangleNormal(v0.pos,v1.pos,v2.pos);
                            xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
                            intersection.st = st;
                            intersection.sceneobject = 0;
                            ray.maxt = intersection.t;
                            intersection.mint = ray3f::epsilon;
                        }
                    }
                }
                if (sptr <= 0) break;
                tie(brick, nodeNum) = stack[--sptr];
            } else if (snode->axis == BRICKNODE) {
                const Lod &lod = lods[snode->lod];
                vec3f center = snode->bound.center();
                float bboxArea = snode->bound.diagonalSqr() * consts<float>::inv_fourpi;
                float r2 = (ray.o - center).lengthSqr();
                float coneArea = sigma2 * r2;
                if(bboxArea < coneArea) {
                    float t = sqrt(r2);
                    if (!hit || t < intersection.t) {
                        hit = true;
                        const Material* material = materials[lod.mindex];
                        const Vertex &vt = lod.vertex;
                        // message_va("p: %s, n: %s, u: %s", vt.pos.print().c_str(), vt.norm.print().c_str(), vt.uv.print().c_str());
                        intersection.t = t;
                        intersection.m = material;
                        intersection.f.o = vt.pos;
                        intersection.f.z = vt.norm;
                        xyFromZ(intersection.f.z,intersection.f.x,intersection.f.y);
                        intersection.st = vt.uv;
                        intersection.mint = snode->bound.diagonal() * 0.5f;
                        intersection.sceneobject = 0;
                        ray.maxt = intersection.t;
                    }
                    if (sptr <= 0) break;
                    tie(brick, nodeNum) = stack[--sptr];
                } else {
                    brick = lod.brick;
                    nodeNum = 0;
                }
            } else {
                if (invDir[snode->axis] < 0) {
                    stack[sptr++] = { cbrick, snode->left };
                    nodeNum = snode->right;
                } else {
                    stack[sptr++] = { cbrick, snode->right };
                    nodeNum = snode->left;
                }
            }
        } else {
            if (sptr <= 0) break;
            tie(brick, nodeNum) = stack[--sptr];
        }
    }
    if (hit && dot(intersection.f.z, ray.d) > 0)
        intersection.f = flip(intersection.f);
    return hit;
}


bool StreamBvhAccelerator::intersectAny(const ray3f& ray) const {
    std::array<pair<uint64_t, uint32_t>, bvhstacksize> stack;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    if(listAccelerator->intersectAny(ray)) return true;

    uint64_t cbrick = -1;
    uint64_t brick = rootBrick;
    int nodeNum = 0;

    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* verts = 0; const Lod *lods = 0;
    while (true) {
        if (cbrick != brick) {
            std::tie(snodes, striangles, verts, lods) = getArrays(brickBuffer + brick);
            cbrick = brick;
        }
        const SNode *snode = &snodes[nodeNum];
        if (intersectBoundingBox(snode->bound, ray, invDir)) {
            if (snode->axis == LEAFNODE) {
                for (int s = 0; s < snode->count; s++) {
                    float t; vec2f uv;
                    const STriangle &tri = striangles[snode->offset + s];
                    const Vertex &v0 = verts[tri.face.x];
                    const Vertex &v1 = verts[tri.face.y];
                    const Vertex &v2 = verts[tri.face.z];
                    bool th = intersectTriangle(v0.pos, v1.pos, v2.pos, ray, t, uv);
                    if(th) {
                        const Material* material = materials[tri.mindex];
                        vec2f st = (tri.ftype & VUV) ? lerp(v0.uv, v1.uv, v2.uv, uv) : uv;
                        if (::opacityResolve(material->opacity, st)) return true;
                    }
                }
                if (sptr <= 0) break;
                tie(brick, nodeNum) = stack[--sptr];
            } else if (snode->type == BRICKNODE) {
                const Lod& lod = lods[snode->lod];
                brick = lod.brick;
                nodeNum = 0; // move to a new brick.
            } else {
                if (invDir[snode->axis] < 0) {
                    stack[sptr++] = { cbrick, snode->left };
                    nodeNum = snode->right;
                } else {
                    stack[sptr++] = { cbrick, snode->right };
                    nodeNum = snode->left;
                }
            }
        } else {
            if (sptr <= 0) break;
            tie(brick, nodeNum) = stack[--sptr];
        }
    }
    return false;
}


float StreamBvhAccelerator::intersectAnyLOD(const ray3f& ray, float sigma2) const {
    std::array<pair<uint64_t, uint32_t>, bvhstacksize> stack;
    vec3f invDir = one3f / ray.d;
    int sptr = 0;

    if(listAccelerator->intersectAny(ray)) return 1.0f;

    uint64_t cbrick = -1;
    uint64_t brick = rootBrick;
    int nodeNum = 0;
    float occlusion = 0.0f;

    const SNode *snodes = 0; const STriangle *striangles = 0;
    const Vertex* verts = 0; const Lod *lods = 0;

    while (true) {
        if (cbrick != brick) {
            std::tie(snodes, striangles, verts, lods) = getArrays(brickBuffer + brick);
            cbrick = brick;
        }
        const SNode *snode = &snodes[nodeNum];
        if (intersectBoundingBox(snode->bound, ray, invDir)) {
            if (snode->type == LEAFNODE) {
                for (int s = 0; s < snode->count; s++) {
                    float t; vec2f uv;
                    const STriangle &tri = striangles[snode->offset + s];
                    const Vertex &v0 = verts[tri.face.x];
                    const Vertex &v1 = verts[tri.face.y];
                    const Vertex &v2 = verts[tri.face.z];
                    bool th = intersectTriangle(v0.pos, v1.pos, v2.pos, ray, t, uv);
                    if(th) {
                        const Material* material = materials[tri.mindex];
                        vec2f st = (tri.ftype & VUV) ? lerp(v0.uv, v1.uv, v2.uv, uv) : uv;
                        if (::opacityResolve(material->opacity, st)) return true;
                    }
                }
                if (sptr <= 0) break;
                tie(brick, nodeNum) = stack[--sptr];
            } else if (snode->type == BRICKNODE) {
                const Lod &lod = lods[snode->lod];
                vec3f center = snode->bound.center();
                float bboxArea = snode->bound.diagonalSqr() * consts<float>::inv_fourpi;;
                float r2 = (ray.o - center).lengthSqr();
                float coneArea = sigma2 * r2;
                if(bboxArea < coneArea) {
                    occlusion += lod.opacity;
                    if (occlusion >= 1.0f) return 1.0f;
                    if (sptr <= 0) break;
                    tie(brick, nodeNum) = stack[--sptr];
                } else {
                    brick = lod.brick;
                    nodeNum = 0; // move to a new brick.
                }
            } else {
                if (invDir[snode->axis] < 0) {
                    stack[sptr++] = { cbrick, snode->left };
                    nodeNum = snode->right;
                } else {
                    stack[sptr++] = { cbrick, snode->right };
                    nodeNum = snode->left;
                }
            }
        } else {
            if (sptr <= 0) break;
            tie(brick, nodeNum) = stack[--sptr];
        }
    }
    return occlusion;
}

void StreamBvhAccelerator::clearAccelerator() {
    if(listAccelerator) listAccelerator->clearAccelerator();
    if(brickBuffer) {
        int e = munmap(brickBuffer, fileLen);
        error_if_not(e == 0, "failed to unmap file");
        brickBuffer = 0;
    }
    if(fd) close(fd); fd = 0;
}

jsonObject StreamBvhAccelerator::statistics() const {
    jsonObject json;
    return json;
}
