#include "accelerator_bvh.h"
#include <scene/transform.h>
#include <scene/shape.h>
#include <algorithm>
#include <tbb/tbb.h>

using namespace BVH;
static const int bvhstacksize = 64;

inline float bboxArea(const range3f &b) {
    vec3f d = b.extent();
    return 2.f * (d.x * d.y + d.x * d.z + d.y * d.z);
}

// Build Items
struct BvhSurfBuildItem {
    range3f     bbox;
    vec3f       centroid;
    int         index;
    static const int count = 1;
};

struct BvhBuildItem {
    vec3f               centroid;
    range3f             bbox;
    int                 count;
    AcceleratorSurface  *surface;
};

struct BvhBuildNode {
    int             axis;
    range3f         bbox;
    union {
        struct {
            int     offset;
            int     nitems;
        };
        BvhBuildNode   *children[2];
    };
};

struct BvhBucket {
    BvhBucket() : count(0), bbox(invalidrange3f){}
    int         count;
    range3f     bbox;
};

template<typename T>
struct CompareToBucket {
    CompareToBucket(int split, int num, int d, const range3f &b)
    : centroidBounds(b), splitBucket(split), nBuckets(num), dim(d) {}
    bool operator()(const T &p) const {
        float r = centroidBounds.max[dim] - centroidBounds.min[dim];
        int b = static_cast<int>(nBuckets *
                                 ((p.centroid[dim] - centroidBounds.min[dim]) / r));
        if (b == nBuckets) b = nBuckets-1;
        error_if_not(b >= 0 && b < nBuckets, "invalid bucket");
        return b <= splitBucket;
    }
    const range3f &centroidBounds;
    int splitBucket, nBuckets, dim;
};

inline LinearBvhNode makeBvhLeaf(BvhSurfBuildItem* ptr, int size, const range3f& bbox,
                                 std::vector<int> &orderedIndices) {
    LinearBvhNode leaf;
    leaf.axis = -1;
    leaf.bbox = bbox;
    leaf.offset = orderedIndices.size();
    leaf.nitems = size;
    for (int p = 0; p < size; p++)
        orderedIndices.push_back(ptr[p].index);
    return leaf;
}

inline LinearBvhNode makeBvhLeaf(BvhBuildItem *ptr, int size, range3f bbox,
                                 std::vector<AcceleratorSurface*> &orderedSurfaces) {
    LinearBvhNode leaf;
    leaf.axis = -1;
    leaf.bbox = bbox;
    leaf.offset = orderedSurfaces.size();
    leaf.nitems = size;
    for (int i = 0; i < size; i++)
        orderedSurfaces.push_back(ptr[i].surface);
    return leaf;
}

inline LinearBvhNode makeBvhBranch(int dim, const range3f &bbox, uint secondChild) {
    LinearBvhNode branch;
    branch.axis = dim;
    branch.bbox = bbox;
    branch.secondChild = secondChild;
    return branch;
}

template<typename R, typename T>
uint recursiveBvhBuild(R *ptr, int size, int maxLeafItems, BvhSplitMode splitMode,
                      std::vector<LinearBvhNode> &nodes, std::vector<T> &orderedSurfaces) {
    error_if(size == 0, "invalid size");
    range3f bbox = invalidrange3f;
    range3f cbbox = invalidrange3f;
    for (int p = 0; p < size; p++) {
        bbox.grow(ptr[p].bbox);
        cbbox.grow(ptr[p].centroid);
    }

    int dim = cbbox.extent().max_component_index();
    if (cbbox.max[dim] == cbbox.min[dim] || size <= maxLeafItems) {
        nodes.push_back(makeBvhLeaf(ptr, size, bbox, orderedSurfaces));
        return nodes.size() - 1;
    } else {
        int mid = 0;
        switch (splitMode) {
            case SPLIT_EVEN: {
                mid = static_cast<int>(size * 0.5f);
                std::nth_element(ptr, ptr + mid, ptr + size,
                                 [dim](const R& a, const R& b)->bool {
                                     float aa = a.centroid[dim];
                                     float bb = b.centroid[dim];
                                     return aa < bb;
                                 });
                if (mid == 0 || mid == size) {
                    nodes.push_back(makeBvhLeaf(ptr, size, bbox, orderedSurfaces));
                    return nodes.size() - 1;
                }
            } break;
            case SPLIT_MIDDLE: {
                float pmid = (cbbox.min[dim] + cbbox.max[dim]) * 0.5f;
                auto *midPtr = std::partition(ptr, ptr + size,
                                              [dim, pmid](const R& a) {
                                                  return a.centroid[dim] < pmid;
                                              });
                mid = (int)(midPtr - ptr);
                if (mid == 0 || mid == size) {
                    nodes.push_back(makeBvhLeaf(ptr, size, bbox, orderedSurfaces));
                    return nodes.size() - 1;
                }
            } break;
            case SPLIT_SAH: {
                const int bucketNum = 32;
                BvhBucket buckets[bucketNum];
                float cost[bucketNum-1];
                for (int p = 0; p < size; ++p) {
                    float range = cbbox.max[dim] - cbbox.min[dim];
                    float offset = ptr[p].centroid[dim] - cbbox.min[dim];
                    int b = min(bucketNum - 1, (int)(bucketNum * (offset / range)));
                    error_if_not(b >= 0 && b < bucketNum, "invalid bucket");
                    buckets[b].count += ptr[p].count;
                    buckets[b].bbox.grow(ptr[p].bbox);
                }

                float totalArea = bboxArea(bbox);
                for (int i = 0; i < bucketNum - 1; ++i) {
                    range3f b0 = invalidrange3f;
                    range3f b1 = invalidrange3f;
                    int count0 = 0;
                    int count1 = 0;
                    for (int j = 0; j <= i; ++j) {
                        b0.grow(buckets[j].bbox);
                        count0 += buckets[j].count;
                    }
                    for (int j = i+1; j < bucketNum; ++j) {
                        b1.grow(buckets[j].bbox);
                        count1 += buckets[j].count;
                    }
                    cost[i] = 0.125f + (count0 * bboxArea(b0) + count1 * bboxArea(b1)) / totalArea;
                }

                float minCost = cost[0]; int minCostSplit = 0;
                for (int i = 1; i < bucketNum - 1; ++i) {
                    if (cost[i] < minCost) {
                        minCost = cost[i];
                        minCostSplit = i;
                    }
                }

                if (size > maxLeafItems || minCost < size) {
                    auto *pmid = std::partition(ptr, ptr + size,
                                                CompareToBucket<R>(minCostSplit, bucketNum, dim, cbbox));
                    mid = (int)(pmid - ptr);
                    if (mid == 0 || mid == size) {
                        nodes.push_back(makeBvhLeaf(ptr, size, bbox, orderedSurfaces));
                        return nodes.size() - 1;
                    }
                } else {
                    mid = 0.5f * size;
                }
            } break;
            default: error("unknown split opt"); break;
        }
        error_if_not(mid != 0 && mid != size, "invalid split");

        int branchNode = nodes.size();
        nodes.emplace_back();
        uint left = recursiveBvhBuild(ptr, mid, maxLeafItems, splitMode, nodes, orderedSurfaces);
        uint right = recursiveBvhBuild(ptr + mid, size - mid, maxLeafItems, splitMode, nodes, orderedSurfaces);
        assert(left = branchNode + 1);
        nodes[branchNode] = makeBvhBranch(dim, bbox, right);
        return branchNode;
    }
}

template<typename T>
AcceleratorAux* buildSurfaceBvhAccelerator(AcceleratorSurface *surface, const BvhAcceleratorOpts &opts,
                                           BvhAcceleratorStat &stat, range3f *surfaceBound, int* count) {
    range3f wbbox = invalidrange3f;
    const T *shape = static_cast<const T*>(surface->shape);
    unsigned int nfaces = shape->face.size();
    std::vector<BvhSurfBuildItem> items(nfaces);
    range3f bbox = invalidrange3f;
    for (unsigned int i = 0; i < nfaces; i++) {
        const vec3i &face = shape->face[i];
        const vec3f &v0 = shape->pos[face.x];
        const vec3f &v1 = shape->pos[face.y];
        const vec3f &v2 = shape->pos[face.z];
        items[i].bbox = triangleBoundingBox(v0, v1, v2);
        bbox.grow(items[i].bbox);
        items[i].index = i;
        items[i].centroid = items[i].bbox.center();

        // get world bounding box
        vec3f wv0 = transformPoint(surface->transform, v0);
        vec3f wv1 = transformPoint(surface->transform, v1);
        vec3f wv2 = transformPoint(surface->transform, v2);
        wbbox.grow(triangleBoundingBox(wv0, wv1, wv2));
    }
    *count = nfaces;
    *surfaceBound = wbbox;

    SurfaceBvh *surfaceBvh = new SurfaceBvh();
    surfaceBvh->indices.reserve(nfaces);
    int root =
        recursiveBvhBuild<BvhSurfBuildItem, int>(items.data(), items.size(), opts.maxLeafItems,
                                                 opts.shapeSplitMode, surfaceBvh->nodes,
                                                 surfaceBvh->indices);
    error_if(root != 0, "invalid node number");
    // compute statistic
    stat.bytes += surfaceBvh->indices.size() * sizeof(int);
    stat.bytes += surfaceBvh->nodes.size() * sizeof(LinearBvhNode);
    stat.surfaceNodeNum += surfaceBvh->nodes.size();
    return surfaceBvh;
}

static void makeSurfaceSubtree(AcceleratorSurface *surface, BvhBuildItem &node,
                               const BvhAcceleratorOpts &opts, BvhAcceleratorStat &stat) {
    range3f bbox = invalidrange3f;
    int count = 0;
    switch(surface->shape->type) {
        case TriangleMeshShape::TYPEID: {
            surface->aux = buildSurfaceBvhAccelerator<TriangleMeshShape>(surface, opts, stat, &bbox, &count);
        } break;
        case HairMeshShape::TYPEID: {
            surface->aux = buildSurfaceBvhAccelerator<HairMeshShape>(surface, opts, stat, &bbox, &count);
        } break;
        case QuadShape::TYPEID: {
            const QuadShape* shape = (const QuadShape*)(surface->shape);
            float w = shape->width / 2.0f;
            float h = shape->height / 2.0f;
            bbox = invalidrange3f;
            bbox.grow(transformPoint(surface->transform, vec3f(-w, -h, 0)));
            bbox.grow(transformPoint(surface->transform, vec3f(-w,  h, 0)));
            bbox.grow(transformPoint(surface->transform, vec3f( w,  h, 0)));
            bbox.grow(transformPoint(surface->transform, vec3f( w, -h, 0)));
            count = 1;
        } break;
        case SphereShape::TYPEID: {
            const SphereShape* shape = (const SphereShape*)(surface->shape);
            float r = shape->radius;
            vec3f center = transformPoint(surface->transform, vec3f(0, 0, 0));
            bbox = range3f(center + vec3f(-r, -r, -r), center + vec3f(r, r, r));
            count = 1;
        } break;
        default: error("unknown geometry type");
    }
    node.bbox = bbox;
    node.surface = surface;
    node.centroid = bbox.center();
    node.count = count;
}

void BvhAccelerator::buildBvhAccelerator() {
    std::vector<BvhBuildItem> buildItems(surfaces.size());

    if (_opts.parallel) {
        tbb::parallel_for(tbb::blocked_range<int>(0, surfaces.size()),
                          [this, &buildItems] (const tbb::blocked_range<int> r) -> void {
                              for (int i = r.begin(); i != r.end(); i++) {
                                  makeSurfaceSubtree(surfaces[i], buildItems[i], _opts, _stat);
                              }
                          });
    } else {
        for (int i = 0; i < surfaces.size(); i++) {
            makeSurfaceSubtree(surfaces[i], buildItems[i], _opts, _stat);
        }
    }

    std::vector<AcceleratorSurface*> orderedSurfaces;
    orderedSurfaces.reserve(surfaces.size());

    int root =
        recursiveBvhBuild<BvhBuildItem, AcceleratorSurface*>(buildItems.data(), buildItems.size(),
                                                             1, _opts.surfaceSplitMode,
                                                             nodes, orderedSurfaces);
    error_if_not(orderedSurfaces.size() == surfaces.size(), "surface number doesn't match!");
    error_if_not(root == 0, "invalid root");
    surfaces = std::move(orderedSurfaces);

    _worldBound = nodes[0].bbox;
    // compute statistic
    _stat.bytes += surfaces.size() * sizeof(AcceleratorSurface);
    _stat.bytes += nodes.size() * sizeof(LinearBvhNode);
    _stat.objectNodeNum += nodes.size();
}

