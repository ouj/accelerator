#include "build.h"
#include "accelerator_list.h"
#include "accelerator_bvh.h"
#include "accelerator_streambvh.h"
#include "accelerator_flatbvh.h"
#include "accelerator_lux.h"
#include "accelerator_mix.h"

AcceleratorOpts* parseAcceleratorOpts(const jsonObject &json) {
    const std::string type = jsonGet(json, "type").as_string();
    AcceleratorOpts* opts = 0;
    if (type == "bvhacceleratoropts") {
        opts = new BvhAcceleratorOpts();
    } else if (type == "streambvhacceleratoropts") {
        opts = new StreamBvhAcceleratorOpts();
    } else if (type == "listacceleratoropts") {
        opts = new ListAcceleratorOpts();
    } else if (type == "luxacceleratoropts") {
        opts = new LuxAcceleratorOpts();
    } else if (type == "mixacceleratoropts") {
        opts = new MixAcceleratorOpts();
    } else if (type == "flatbvhacceleratoropts") {
        opts = new FlatBvhAcceleratorOpts();
    } else {
        error("unknown accelerator type");
    }
    if (opts) opts->parse(json);
    return opts;
}

Accelerator* createAccelerator(const AcceleratorOpts *opts) {
    switch(opts->type) {
        case ListAccelerator::TYPEID:
            return new ListAccelerator(*(ListAcceleratorOpts*)opts);
        case BvhAccelerator::TYPEID:
            return new BvhAccelerator(*(BvhAcceleratorOpts*)opts);
        case FlatBvhAccelerator::TYPEID:
            return new FlatBvhAccelerator(*(FlatBvhAcceleratorOpts*)opts);
        case LuxAccelerator::TYPEID:
            return new LuxAccelerator(*(LuxAcceleratorOpts*)opts);
        case MixAccelerator::TYPEID:
            return new MixAccelerator(*(MixAcceleratorOpts*)opts);
        case StreamBvhAccelerator::TYPEID:
            return new StreamBvhAccelerator(*(StreamBvhAcceleratorOpts*)opts);
        default:
            error("unknown accelerator type");
    }
    return nullptr;
}


Accelerator* createAccelerator(const jsonObject &json) {
    std::unique_ptr<AcceleratorOpts> opts(parseAcceleratorOpts(json));
    return createAccelerator(opts.get());
}

Accelerator* createDefaultAccelerator() {
    return new BvhAccelerator();
}
