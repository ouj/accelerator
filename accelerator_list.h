#ifndef ACCELERATOR_LIST_H
#define ACCELERATOR_LIST_H

#include "accelerator.h"

struct ListAcceleratorOpts : AcceleratorOpts {
    ListAcceleratorOpts();
    jsonObject          print() const;
    void                parse(const jsonObject &opts);
};

class ListAccelerator : public Accelerator {
public:
    static const int TYPEID = 30001;
    ListAccelerator();
    ListAccelerator(const ListAcceleratorOpts &opts);
    virtual ~ListAccelerator() { clearAccelerator(); };

    virtual bool        intersectFirst(const ray3f& ray, sintersection3f& intersection,
                                       AcceleratorSurface*& surface) const;
    virtual bool        intersectFirst(const ray3f& ray, intersection3f& intersection) const ;
    virtual bool        intersectAny(const ray3f& ray) const;
    virtual range3f     computeWorldBound() const;

    virtual bool        buildAccelerator(const std::vector<Surface*>& surfaces,
                                         const std::vector<Light*>& lights);
    virtual void        clearAccelerator();

    virtual jsonObject  statistics() const;
private:
    range3f                             _worldBound = invalidrange3f;
    std::vector<AcceleratorSurface*>    _surfaces;
    ListAcceleratorOpts                 _opts;
};

#endif
